import UIKit
import Storage

var str = "Hello, Storage"

struct Timeline: Codable {
    let tweets: [String]
}

let path = URL(fileURLWithPath: NSTemporaryDirectory())

print(path)

let pathD = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
let d = pathD?.appendingPathComponent("images", isDirectory: true)

print(pathD)
print(d)

let diskStorage = DiskStorage(path: path)
let codableStorage = CodableStorage(storage: diskStorage)

let timeline = Timeline(
    tweets: ["Hello", "World", "!!!"]
)

print(timeline)

try codableStorage.save(timeline, for: "timeline")
let cached: Timeline = try codableStorage.fetch(for: "timeline")

print(cached)
