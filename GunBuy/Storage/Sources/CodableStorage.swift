/**

 Хранилище для struct...

 Использование:

 struct Timeline: Codable {
 let tweets: [String]
 }

 let path = URL(fileURLWithPath: NSTemporaryDirectory())
 let disk = DiskStorage(path: path)
 let storage = CodableStorage(storage: disk)

 let timeline = Timeline(tweets: ["Hello", "World", "!!!"])
 try storage.save(timeline, for: "timeline")
 let cached: Timeline = try storage.fetch(for: "timeline")

*/

import Foundation

public class CodableStorage {

    private let storage: DiskStorage
    private let decoder: JSONDecoder
    private let encoder: JSONEncoder

    public init(storage: DiskStorage, decoder: JSONDecoder = .init(), encoder: JSONEncoder = .init()) {
        self.storage = storage
        self.decoder = decoder
        self.encoder = encoder
    }

    public func fetch<T: Decodable>(for key: String) throws -> T {
        let data = try storage.fetchValue(for: key)
        return try decoder.decode(T.self, from: data)
    }

    public func save<T: Encodable>(_ value: T, for key: String) throws {
        let data = try encoder.encode(value)
        try storage.save(value: data, for: key)
    }

}
