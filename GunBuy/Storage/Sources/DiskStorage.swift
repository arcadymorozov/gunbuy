/**

 Сохранение данных на диске

 Использование:

 let path = URL(fileURLWithPath: NSTemporaryDirectory())
 let storage = DiskStorage(path: path)


 try storage.save(value: data, for: key)
 let data = try storage.fetchValue(for: key)

 */

import Foundation

public typealias Handler<T> = (Result<T, Error>) -> Void
public typealias Storage = ReadableStorage & WritableStorage

public protocol ReadableStorage {
    func fetchValue(for key: String) throws -> Data
    func fetchValue(for key: String, handler: @escaping Handler<Data>)
}

public enum StorageError: Error {
    case notFound
    case cantWrite(Error)
}

public protocol WritableStorage {
    func save(value: Data, for key: String) throws
    func save(value: Data, for key: String, handler: @escaping Handler<Data>)
}

public class DiskStorage {

    private let queue: DispatchQueue
    private let fileManager: FileManager
    private let path: URL

    public init(path: URL, queue: DispatchQueue = DispatchQueue(label: "DiskCache.Queue"), fileManager: FileManager = FileManager.default) {
        self.path = path
        self.queue = queue
        self.fileManager = fileManager
    }

}

extension DiskStorage: WritableStorage {

    public func save(value: Data, for key: String) throws {
        let url = path.appendingPathComponent(key)
        do {
            try self.createFolders(in: url)
            try value.write(to: url, options: .atomic)
        } catch {
            throw StorageError.cantWrite(error)
        }
    }

    public func save(value: Data, for key: String, handler: @escaping Handler<Data>) {
        queue.async {
            do {
                try self.save(value: value, for: key)
                handler(.success(value))
            } catch {
                handler(.failure(error))
            }
        }
    }

}

extension DiskStorage {

    private func createFolders(in url: URL) throws {
        let folderUrl = url.deletingLastPathComponent()
        if !fileManager.fileExists(atPath: folderUrl.path) {
            try fileManager.createDirectory(at: folderUrl, withIntermediateDirectories: true, attributes: nil)
        }
    }

}

extension DiskStorage: ReadableStorage {

    public func fetchValue(for key: String) throws -> Data {
        let url = path.appendingPathComponent(key)
        guard let data = fileManager.contents(atPath: url.path) else {
            throw StorageError.notFound
        }
        return data
    }

    public func fetchValue(for key: String, handler: @escaping Handler<Data>) {
        queue.async {
            handler(Result {
                try self.fetchValue(for: key)
            })
        }
    }

}
