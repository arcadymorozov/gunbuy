
import UIKit
import CoreData

let SupplierDetailsDataContextDidChangeSuccessfuly = Notification.Name("SupplierDetailsDataContextDidChangeSuccessfuly")

class SupplierDetailsDataContext {

    var supplier: Supplier?

    /** Контроллер для извлечения данных */
    var fetchedResultsController: NSFetchedResultsController<Product>!

    /** Контроллер доступа к базе */
    var dataController: CoreDataController!

    var title: String {
        return supplier?.name ?? "Supplier".localized
    }

    var isEmpty: Bool {
        if let sections = fetchedResultsController.sections, sections.isEmpty {
            return true
        }
        return false
    }

    init(with supplier: Supplier?, dataController: CoreDataController) {
        self.supplier = supplier
        self.dataController = dataController
        self.fetchedResultsController = Product.fetchedResultsController(with: dataController.viewContext, supplier: supplier)
    }

    /** Извлечение данных */
    func fetchData() {
        do {
            try fetchedResultsController.performFetch()
        } catch {
            print("Showcase fetch error")
        }
    }

}

extension SupplierDetailsDataContext: TableDataCollection {

    func registerCells(for tableView: UITableView) {
        ProductTableViewCellModel.registerCell(for: tableView)
    }

    func numberOfSections(for tableView: UITableView) -> Int {
        return fetchedResultsController.sections?.count ?? 0
    }

    func numberOfRows(for tableView: UITableView, in section: Int) -> Int {
        return fetchedResultsController.sections?[section].numberOfObjects ?? 0
    }

    func dequeueReusableCell(for tableView: UITableView, in viewController: UIViewController?, at indexPath: IndexPath) -> UITableViewCell {
        let cell = ProductTableViewCellModel.cell(for: tableView, at: indexPath)
        let supplier = fetchedResultsController.object(at: indexPath)
        ProductTableViewCellModel.setup(cell, with: supplier)
        return cell
    }

    func titleForHeader(for tableView: UITableView, in section: Int) -> String? {
        return fetchedResultsController.sections?[section].name
    }

    /**
     Выбор пункта меню. Открытие контакта.
     */
    func didSelect(for tableView: UITableView, in viewController: UIViewController?, at indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let product = fetchedResultsController.object(at: indexPath)
        if let identifier = product.identifier {
            let dataContext = dataController.newBackgroundContext()
            let product = Product.product(identifier: identifier, in: dataContext)
            let modifyViewController = UIStoryboard.Products.modify(product: product, dataContext: dataContext, image: nil, isModal: false, delegate: self).viewController
            viewController?.navigationController?.pushViewController(modifyViewController, animated: true)
        }
    }

}

extension SupplierDetailsDataContext {

    func registerCells(for collectionView: UICollectionView) {
        ProductCollectionViewCellModel.registerCell(for: collectionView)
    }

    func numberOfItems(for collectionView: UICollectionView, in section: Int) -> Int {
        return fetchedResultsController.sections?[section].numberOfObjects ?? 0
    }

    func dequeueReusableCell(for collectionView: UICollectionView, in viewController: UIViewController?, at indexPath: IndexPath) -> UICollectionViewCell {
        let cell = ProductCollectionViewCellModel.cell(for: collectionView, at: indexPath)
        let supplier = fetchedResultsController.object(at: indexPath)
        ProductCollectionViewCellModel.setup(cell, with: supplier)
        return cell
    }

    /**
     Выбор пункта меню. Открытие контакта.
     */
    func didSelect(for collectionView: UICollectionView, in viewController: UIViewController?, at indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        //let product = fetchedResultsController.object(at: indexPath)
        //viewController?.navigationController?.pushViewController(UIStoryboard.Products.details(product: product).viewController, animated: true)
    }

}

extension SupplierDetailsDataContext {

    /** Создание продукта */
    func createProduct(image: UIImage?, preview: UIImage?, presenter: UIViewController?) {
        // Создаем товар
        let dataContext = dataController.newBackgroundContext()
        let product = Product.create(in: dataContext)
        product.picture = preview
        if let identifier = supplier?.identifier {
            product.supplier = Supplier.supplier(identifier: identifier, in: dataContext)
        }

        let modifyViewController = UIStoryboard.Products.modify(product: product, dataContext: dataContext, image: image, isModal: true, delegate: self).viewController
        //presenter.present(modifyViewController, animated: true)
        presenter?.navigationController?.pushViewController(modifyViewController, animated: true)
    }

    /**
     Удаление товара
    */
    func removeProduct(at indexPath: IndexPath) {
        let product = fetchedResultsController.object(at: indexPath)
        dataController.viewContext.delete(product)
        dataController.save()
        NotificationCenter.default.post(name: SupplierDetailsDataContextDidChangeSuccessfuly, object: nil)
    }

    /**
     Редактирование  производителя

     - parameter presenter: Контроллер, в котором будет открыта форма редактирования
     */
    func modifySupplier(presenter: UIViewController) {
        let dataContext = dataController.newBackgroundContext()
        // Достаем выставку
        if let identifier = supplier?.identifier {
            let supplier = Supplier.supplier(identifier: identifier, in: dataContext)
            let modifyViewController = UIStoryboard.Suppliers.modify(supplier: supplier, dataContext: dataContext, image: nil, delegate: (presenter as! SupplierModifyViewControllerDelegate)).viewController
            let navigationController = UINavigationController(rootViewController: modifyViewController)
            presenter.present(navigationController, animated: true)
        }
    }

}

extension SupplierDetailsDataContext: ProductModifyViewControllerDelegate {

    func productModifyViewController(_ productModifyViewController: ProductModifyViewController, didChange product: Product?, in dataContext: NSManagedObjectContext) {
        dataController.save(changesIn: dataContext)
        NotificationCenter.default.post(name: SupplierDetailsDataContextDidChangeSuccessfuly, object: nil)
        if productModifyViewController.isModal {
            productModifyViewController.dismiss(animated: true)
        } else {
            productModifyViewController.navigationController?.popViewController(animated: true)
        }
    }

    func productModifyViewControllerWillCancel(_ productModifyViewController: ProductModifyViewController) {
        productModifyViewController.dismiss(animated: true)
    }

}

extension SupplierDetailsDataContext {

    func updateSearchResults(with searchText: String, in viewController: UIViewController) {
        let request = fetchedResultsController.fetchRequest
        if searchText.isEmpty {
            request.predicate = nil
        } else {
            request.predicate = NSPredicate(format: "name contains[c] %@", searchText)
        }
        fetchData()

        if let resultsController = viewController as? UITableViewController {
            resultsController.tableView.reloadData()
        }
    }

}
