/**

 Агрегатор управления для табличного вида.

 Занимаетмся предоставлением данных для табличного отображени,
 обработкой взаимодействий с элементами табличного вида.

 */

import UIKit

class TableViewCoordinator: NSObject {

    /**
     Коллекция данных для табличного вида
     */
    let dataCollection: TableDataCollection

    /**
     Контроллер, в котором отображается табличный вид.
     */
    let viewController: UIViewController?

    /**
     Создание координатора табличного вида
     - parameter dataCollection: Коллекция данных, которой будут делегироваться запросы данных
     - parameter viewController: ViewController для отображения табличного вида
     */
    init(with dataCollection: TableDataCollection, viewController: UIViewController?) {
        self.dataCollection = dataCollection
        self.viewController = viewController
    }

    /**
     Добавление табличного вида
     - parameter tableView: Табличный вид, для которого будут предоставляться данные
     */
    func add(_ tableView: UITableView) {
        tableView.delegate = self
        tableView.dataSource = self
        dataCollection.registerCells(for: tableView)
    }

}

extension TableViewCoordinator: UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return dataCollection.numberOfSections(for: tableView)
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataCollection.numberOfRows(for: tableView, in: section)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return dataCollection.dequeueReusableCell(for: tableView, in: viewController, at: indexPath)
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return dataCollection.titleForHeader(for: tableView, in: section)
    }

    //MARK: - Editing Style

    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return dataCollection.editingStyle(for: tableView, at: indexPath)
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        dataCollection.commitEditingStyle(editingStyle, for: tableView, in: viewController, at: indexPath)
    }

    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return dataCollection.canEditRow(for: tableView, at: indexPath)
    }

}

extension TableViewCoordinator: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        dataCollection.didSelect(for: tableView, in: viewController, at: indexPath)
    }

}
