
import UIKit.UIColor

extension UIColor {

    convenience init(hexString: String) {
        var hex = hexString

        let index = hex.firstIndex(of: "#") ?? hex.startIndex
        hex = String(hex[index...])

        let redHex = String(hex[hex.startIndex...hex.index(hex.startIndex, offsetBy: 1)])
        let greenHex = String(hex[hex.index(hex.startIndex, offsetBy: 2)...hex.index(hex.startIndex, offsetBy: 3)])
        let blueHex = String(hex[hex.index(hex.startIndex, offsetBy: 4)...hex.index(hex.startIndex, offsetBy: 5)])

        var redInt: CUnsignedInt = 0
        var greenInt: CUnsignedInt = 0
        var blueInt: CUnsignedInt = 0

        Scanner(string: redHex).scanHexInt32(&redInt)
        Scanner(string: greenHex).scanHexInt32(&greenInt)
        Scanner(string: blueHex).scanHexInt32(&blueInt)

        self.init(red: CGFloat(redInt) / 255.0, green: CGFloat(greenInt) / 255.0, blue: CGFloat(blueInt) / 255.0, alpha: 1.0)
    }

}
