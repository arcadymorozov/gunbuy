/**
 
 Авторизация
 
 */

import UIKit
import CoreData

extension UIStoryboard {
    
    enum Authorization {
        case main
        
        var storyboard: UIStoryboard {
            return UIStoryboard(name: "Authorization", bundle: nil)
        }
        
        var name: String {
            switch self {
            case .main: return "AuthorizationViewController"
            }
        }
        
        var viewController: UIViewController {
            switch self {
            case .main:
                guard let authorizationViewController = self.storyboard.instantiateViewController(withIdentifier: self.name) as? AuthorizationViewController else {
                    fatalError("AuthorizationViewController not found")
                }
                let navigationController = UINavigationController(rootViewController: authorizationViewController)
                return navigationController
            }
        }
    }
    
}
