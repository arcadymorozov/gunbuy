
import Foundation

extension NSString{

    func firstChar() -> String {
        if self.length == 0 {
            return ""
        }
        return self.substring(to: 1)
    }

}
