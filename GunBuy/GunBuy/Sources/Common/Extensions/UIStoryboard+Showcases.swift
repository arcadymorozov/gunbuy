/**

 Showcases Storyboard

 */

import UIKit
import CoreData

extension UIStoryboard {

    enum Showcases {
        case main(dataController: CoreDataController)
        case sources(delegate: ShowcaseSourceViewControllerDelegate)
        case modify(showcase: Showcase?, dataContext: NSManagedObjectContext, delegate: ShowcaseModifyViewControllerDelegate?)

        var storyboard: UIStoryboard {
            return UIStoryboard(name: "Showcases", bundle: nil)
        }

        var name: String {
            switch self {
            case .main: return "ShowcasesViewController"
            case .sources: return "ShowcaseSourceViewController"
            case .modify: return "ShowcaseModifyViewController"
            }
        }

        var viewController: UIViewController {
            switch self {
            case .main(let dataController):
                guard let showcasesViewController = self.storyboard.instantiateViewController(withIdentifier: self.name) as? ShowcasesViewController else {
                    fatalError("ShowcasesViewController not found")
                }
                showcasesViewController.context = ShowcasesDataContext(dataController: dataController)
                let navigationController = UINavigationController(rootViewController: showcasesViewController)
                return navigationController

            case .sources(let delegate):
                guard let sourceViewController = self.storyboard.instantiateViewController(withIdentifier: self.name) as? ShowcaseSourceViewController else {
                    fatalError("ShowcaseSourceViewController not found")
                }
                sourceViewController.delegate = delegate
                return sourceViewController

            case .modify(let showcase, let dataContext, let delegate):
                guard let modifyViewController = self.storyboard.instantiateViewController(withIdentifier: self.name) as? ShowcaseModifyViewController else {
                    fatalError("ShowcaseModifyViewController not found")
                }
                modifyViewController.delegate = delegate
                let context = ShowcaseModifyDataContext(with: showcase, in: dataContext)
                modifyViewController.context = context
                let navigationController = UINavigationController(rootViewController: modifyViewController)
                return navigationController
            }
        }
    }

}
