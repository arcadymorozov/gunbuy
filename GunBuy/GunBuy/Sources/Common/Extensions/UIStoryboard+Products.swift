/**

 Товары

 */

import UIKit
import CoreData

extension UIStoryboard {

    enum Products {
        case main(showcase: Showcase, dataController: CoreDataController)
        case modify(product: Product?, dataContext: NSManagedObjectContext, image: UIImage?, isModal: Bool, delegate: ProductModifyViewControllerDelegate?)
        case shoot(delegate: CaptureMediaViewControllerDelegate?)
        case properties(delegate: PropertiesViewControllerDelegate?)

        var storyboard: UIStoryboard {
            return UIStoryboard(name: "Products", bundle: nil)
        }

        var name: String {
            switch self {
            case .main: return "ProductsViewController"
            case .modify: return "ProductModifyViewController"
            case .shoot: return "ProductCapturePhotoViewController"
            case .properties: return "PropertiesViewController"
            }
        }

        var viewController: UIViewController {
            switch self {
            case .main(let showcase, let dataController):
                guard let mainViewController = self.storyboard.instantiateViewController(withIdentifier: self.name) as? ProductsViewController else {
                    fatalError("ProductsViewController not found")
                }
                mainViewController.context = ProductsDataContext(with: showcase, dataController: dataController)
                return mainViewController

            case .modify(let product, let dataContext, let image, let isModal, let delegate):
                guard let modifyViewController = self.storyboard.instantiateViewController(withIdentifier: self.name) as? ProductModifyViewController else {
                    fatalError("ProductModifyViewController not found")
                }
                modifyViewController.delegate = delegate
                modifyViewController.context = ProductModifyDataContext(with: product, in: dataContext, image: image, isModal: isModal)
                //let navigationController = UINavigationController(rootViewController: modifyViewController)
                //navigationController.navigationBar.prefersLargeTitles = true
                //return navigationController
                return modifyViewController

            case .shoot(let delegate):
                guard let shootViewController = self.storyboard.instantiateViewController(withIdentifier: self.name) as? ProductCapturePhotoViewController else {
                    fatalError("ProductCaptureMediaViewController not found")
                }
                shootViewController.delegate = delegate
                let navigationController = UINavigationController(rootViewController: shootViewController)
                return navigationController

            case .properties(let delegate):
                guard let propertiesViewController = self.storyboard.instantiateViewController(withIdentifier: self.name) as? PropertiesViewController else {
                    fatalError("PropertiesViewController not found")
                }
                propertiesViewController.delegate = delegate
                return propertiesViewController
            }
        }
    }

}
