
import UIKit.UIColor

extension UIColor {

    enum ColorType {
        case gray
        case lightGray
        case red
        case green
        case white
        case yellow
        case black
    }

    convenience init(_ colorType: ColorType) {
        switch colorType {
        case .gray:
            self.init(hexString: "979797")
        case .lightGray:
            self.init(hexString: "C7C8CA")
        case .red:
            self.init(hexString: "FF1D1D")
        case .green:
            self.init(hexString: "6CA924")
        case .white:
            self.init(hexString: "FDFEFE")
        case .yellow:
            self.init(hexString: "FFC107")
        case .black:
            self.init(hexString: "000000")
        }
    }

}
