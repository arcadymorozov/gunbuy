/**

  Поставщики

 */

import UIKit
import CoreData

extension UIStoryboard {

    enum Suppliers {
        case main(showcase: Showcase, dataController: CoreDataController)
        case modify(supplier: Supplier?, dataContext: NSManagedObjectContext, image: UIImage?, delegate: SupplierModifyViewControllerDelegate?)
        case shoot(delegate: CaptureMediaViewControllerDelegate?)
        case details(supplier: Supplier?, dataController: CoreDataController)

        var storyboard: UIStoryboard {
            return UIStoryboard(name: "Suppliers", bundle: nil)
        }

        var name: String {
            switch self {
            case .main: return "SuppliersViewController"
            case .modify: return "SupplierModifyViewController"
            case .shoot: return "SupplierCapturePhotoViewController"
            case .details: return "SupplierDetailsViewController"
            }
        }

        var viewController: UIViewController {
            switch self {
            case .main(let showcase, let dataController):
                guard let mainViewController = self.storyboard.instantiateViewController(withIdentifier: self.name) as? SuppliersViewController else {
                    fatalError("SuppliersViewController not found")
                }
                mainViewController.context = SuppliersDataContext(with: showcase, dataController: dataController)
                return mainViewController

            case .modify(let supplier, let dataContext, let image, let delegate):
                guard let modifyViewController = self.storyboard.instantiateViewController(withIdentifier: self.name) as? SupplierModifyViewController else {
                    fatalError("SupplierModifyViewController not found")
                }
                modifyViewController.delegate = delegate
                modifyViewController.context = SupplierModifyDataContext(with: supplier, in: dataContext, image: image)
                return modifyViewController

            case .shoot(let delegate):
                guard let shootViewController = self.storyboard.instantiateViewController(withIdentifier: self.name) as? SupplierCapturePhotoViewController else {
                    fatalError("SupplierCapturePhotoViewController not found")
                }
                shootViewController.delegate = delegate
                let navigationController = UINavigationController(rootViewController: shootViewController)
                return navigationController

            case .details(let supplier, let dataController):
                guard let detailsViewController = self.storyboard.instantiateViewController(withIdentifier: self.name) as? SupplierDetailsViewController else {
                    fatalError("SupplierDetailsViewController not found")
                }
                detailsViewController.context = SupplierDetailsDataContext(with: supplier, dataController: dataController)
                return detailsViewController
            }
        }
    }

}
