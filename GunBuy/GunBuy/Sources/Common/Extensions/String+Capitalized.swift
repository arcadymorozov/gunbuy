
extension String {

    func capitalized() -> String {
        return prefix(1).uppercased() + self.lowercased().dropFirst()
    }

    mutating func capitalized() {
        self = self.capitalized()
    }

}
