/**

 Sketch

 */

import UIKit
import CoreData

extension UIStoryboard {

    enum Sketch {
        case main(image: UIImage?, delegate: SketchViewControllerDelegate?)

        var storyboard: UIStoryboard {
            return UIStoryboard(name: "Sketch", bundle: nil)
        }

        var name: String {
            switch self {
            case .main: return "SketchViewController"
            }
        }

        var viewController: UIViewController {
            switch self {
            case .main(let image, let delegate):
                guard let sketchViewController = self.storyboard.instantiateViewController(withIdentifier: self.name) as? SketchViewController else {
                    fatalError("SketchViewController not found")
                }
                sketchViewController.context = SketchDataContext(with: image)
                sketchViewController.delegate = delegate
                let navigationController = UINavigationController(rootViewController: sketchViewController)
                return navigationController
            }
        }
    }

}
