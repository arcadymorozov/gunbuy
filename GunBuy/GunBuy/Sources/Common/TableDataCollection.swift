/**

 Предоставление данных для табличного отображения UITableView

 */

import UIKit

protocol TableDataCollection {

    func registerCells(for tableView: UITableView)

    func numberOfSections(for tableView: UITableView) -> Int

    func numberOfRows(for tableView: UITableView, in section: Int) -> Int

    func dequeueReusableCell(for tableView: UITableView, in viewController: UIViewController?, at indexPath: IndexPath) -> UITableViewCell

    // MARK: - DataSource Optionals

    func titleForHeader(for tableView: UITableView, in section: Int) -> String?

    // MARK: - Delegate Optionals

    func didSelect(for tableView: UITableView, in viewController: UIViewController?, at indexPath: IndexPath)

    // MARK: - Editing style Optionals

    func commitEditingStyle(_ editingStyle: UITableViewCell.EditingStyle, for tableView: UITableView, in viewController: UIViewController?, at indexPath: IndexPath)

    func editingStyle(for tableView: UITableView, at indexPath: IndexPath) -> UITableViewCell.EditingStyle

    func canEditRow(for tableView: UITableView, at indexPath: IndexPath) -> Bool

    // MARK: - Search

    func updateSearchResults(with searchText: String, in viewController: UIViewController)

}

// MARK: - Optionals func body -

extension TableDataCollection {

    func didSelect(for tableView: UITableView, in viewController: UIViewController?, at indexPath: IndexPath) {
    }

    func titleForHeader(for tableView: UITableView, in section: Int) -> String? {
        return nil
    }

    func commitEditingStyle(_ editingStyle: UITableViewCell.EditingStyle, for tableView: UITableView, in viewController: UIViewController?, at indexPath: IndexPath) {
    }

    func editingStyle(for tableView: UITableView, at indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .none
    }

    func canEditRow(for tableView: UITableView, at indexPath: IndexPath) -> Bool {
        return false
    }

    func updateSearchResults(with searchText: String, in viewController: UIViewController) {
    }

}
