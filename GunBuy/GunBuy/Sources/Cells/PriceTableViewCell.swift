
import UIKit

protocol PriceTableViewCellDelegate: class {

    /**
     Начало редактиврования тектсового поля

     - parameter cell: Ячейка таблицы, в которой начато редактирование textField
     - parameter textField: Текстовое поле, в котором начато редактирование
     - parameter indexPath: Текущий IndexPath для ячейки cell
     */
    func priceTableViewCell(_ cell: PriceTableViewCell, didBeginEditing textField: UITextField, at indexPath: IndexPath?)

    func priceTableViewCell(_ cell: PriceTableViewCell, didChangeCost value: String?, at indexPath: IndexPath?)

    func priceTableViewCell(_ cell: PriceTableViewCell, didChangeCurrency value: String?, at indexPath: IndexPath?)

}

class PriceTableViewCell: UITableViewCell {

    static var identifier: String {
        return "PriceTableViewCell"
    }

    weak var delegate: PriceTableViewCellDelegate?

    var indexPath: IndexPath?

    @IBOutlet weak var costSurfaceView: UIView! {
        didSet {
            costSurfaceView.layer.cornerRadius = 5
            costSurfaceView.layer.masksToBounds = true
            costSurfaceView.layer.borderColor = UIColor.groupTableViewBackground.cgColor
            costSurfaceView.layer.borderWidth = 1.0
        }
    }

    @IBOutlet weak var currencySurfaceView: UIView! {
        didSet {
            currencySurfaceView.layer.cornerRadius = 5
            currencySurfaceView.layer.masksToBounds = true
            currencySurfaceView.layer.borderColor = UIColor.groupTableViewBackground.cgColor
            currencySurfaceView.layer.borderWidth = 1.0
        }
    }

    @IBOutlet weak var costTextField: UITextField!

    @IBOutlet weak var currencyTextField: UITextField!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func resignResponder() {
        costTextField.resignFirstResponder()
    }

    func becomeResponder() {
        costTextField.becomeFirstResponder()
    }

}

extension PriceTableViewCell: UITextFieldDelegate {

    func textFieldDidBeginEditing(_ textField: UITextField) {
        delegate?.priceTableViewCell(self, didBeginEditing: textField, at: indexPath)
    }

    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        if textField == costTextField {
            delegate?.priceTableViewCell(self, didChangeCost: textField.text, at: indexPath)
        } else {
            delegate?.priceTableViewCell(self, didChangeCurrency: textField.text, at: indexPath)
        }
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

}
