/**

 Товар
 Ячейка коллекции

 */

import UIKit

class ProductCollectionViewCell: UICollectionViewCell {

    static var identifier: String {
        return "ProductCollectionViewCell"
    }

    var indexPath: IndexPath?

    @IBOutlet weak var nameLabel: UILabel!

    @IBOutlet weak var iconImageView: UIImageView! {
        didSet {
            iconImageView.layer.cornerRadius = 5
            iconImageView.layer.masksToBounds = true
            iconImageView.layer.borderColor = UIColor.groupTableViewBackground.cgColor
            iconImageView.layer.borderWidth = 1.0
        }
    }

}
