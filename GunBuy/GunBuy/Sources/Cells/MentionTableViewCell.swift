/**

 Медиа контент

 */

import UIKit

protocol MentionTableViewCellDelegate: class {

    func mentionTableViewCell(_ cell: MentionTableViewCell, didBeginEditing responder: UIResponder, at indexPath: IndexPath?)

    func mentionTableViewCell(_ cell: MentionTableViewCell, didChange value: AnyObject?, at indexPath: IndexPath?)

}

class MentionTableViewCell: UITableViewCell {

    class var identifier: String {
        return "MentionTableViewCell"
    }

    weak var delegate: MentionTableViewCellDelegate?

    weak var tableView: UITableView?

    var indexPath: IndexPath?

}
