/**

 Редактирование строки текста

 */

import UIKit

class TextFieldTableViewCellModel {

    /**
     Установка значения ячейки Редактирования свойства.
     - parameter cell: Ячейка редактирования текстового свойства.
     - parameter title: Название свойства.
     - parameter value: Значения текстового свойства.
     - parameter style: Стиль текстового поля TextFieldStyle. Поумолчанию Текст.
     - parameter delegate: Получатель событий TextFieldTableViewCellDelegate.
     - parameter indexPath: Адрес ячейки.
     */
    class func setup(_ cell: TextFieldTableViewCell, title: String, value: String?, style: TextFieldStyle = .text, delegate: TextFieldTableViewCellDelegate? = nil, at indexPath: IndexPath? = nil) {
        cell.delegate = delegate
        cell.indexPath = indexPath
        cell.valueTextField.placeholder = title
        cell.valueTextField.text = value
        switch style {
        case .text: cell.valueTextField.keyboardType = .default
        case .email: cell.valueTextField.keyboardType = .emailAddress
        case .number: cell.valueTextField.keyboardType = .numbersAndPunctuation
        }
    }

    /**
     Регистрирование ячейки Редактирования свойства в tableView.
     - parameter tableView: UITableView для которой регистрируется ячейка.
     */
    class func registerCell(for tableView: UITableView) {
        tableView.register(UINib(nibName: TextFieldTableViewCell.identifier, bundle: nil), forCellReuseIdentifier: TextFieldTableViewCell.identifier)
    }

    /**
     Извлечение ячейки Редактирования свойства в tableView.
     - parameter tableView: UITableView для которой извлекается ячейка.
     - parameter indexPath: Адрес ячейки.
     */
    class func cell(for tableView: UITableView, at indexPath: IndexPath) -> TextFieldTableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: TextFieldTableViewCell.identifier, for: indexPath) as? TextFieldTableViewCell else {
            fatalError("TextFieldTableViewCell dequeue error")
        }
        return cell
    }

}
