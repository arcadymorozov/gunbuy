
import UIKit

class SupplierTableViewCellModel {

    /**
     Установка значения ячейки Редактирования свойства.
     - parameter cell: Ячейка редактирования текстового свойства.
     - parameter product: Название..
     - parameter indexPath: Адрес ячейки.
     */
    class func setup(_ cell: SupplierTableViewCell, with supplier: Supplier, at indexPath: IndexPath? = nil) {
        cell.indexPath = indexPath
        cell.nameLabel.text = supplier.name
        cell.detailsLabel.text = supplier.details
        cell.iconImageView.image = supplier.picture

        if let count = supplier.products?.count, count > 0 {
            cell.countLabel.text = "\(count)"
        } else {
            cell.countLabel.text = nil
        }
    }

    /**
     Регистрирование ячейки Редактирования свойства в tableView.
     - parameter tableView: UITableView для которой регистрируется ячейка.
     */
    class func registerCell(for tableView: UITableView) {
        tableView.register(UINib(nibName: SupplierTableViewCell.identifier, bundle: nil), forCellReuseIdentifier: SupplierTableViewCell.identifier)
    }

    /**
     Извлечение ячейки Редактирования свойства в tableView.
     - parameter tableView: UITableView для которой извлекается ячейка.
     - parameter indexPath: Адрес ячейки.
     */
    class func cell(for tableView: UITableView, at indexPath: IndexPath) -> SupplierTableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: SupplierTableViewCell.identifier, for: indexPath) as? SupplierTableViewCell else {
            fatalError("SupplierTableViewCell dequeue error")
        }
        return cell
    }

}
