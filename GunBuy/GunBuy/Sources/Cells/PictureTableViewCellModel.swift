
import UIKit

class PictureTableViewCellModel {

    /**
     */
    class func setup(_ cell: PictureTableViewCell, with image: UIImage?, text: String?, delegate: PictureTableViewCellDelegate, at indexPath: IndexPath? = nil) {
        cell.indexPath = indexPath
        cell.delegate = delegate
        cell.textView.text = text
        cell.pictureImageView.image = image
    }

    /**
     Регистрирование ячейки Редактирования свойства в tableView.
     - parameter tableView: UITableView для которой регистрируется ячейка.
     */
    class func registerCell(for tableView: UITableView) {
        tableView.register(UINib(nibName: PictureTableViewCell.identifier, bundle: nil), forCellReuseIdentifier: PictureTableViewCell.identifier)
    }

    /**
     Извлечение ячейки Редактирования свойства в tableView.
     - parameter tableView: UITableView для которой извлекается ячейка.
     - parameter indexPath: Адрес ячейки.
     */
    class func cell(for tableView: UITableView, at indexPath: IndexPath) -> PictureTableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: PictureTableViewCell.identifier, for: indexPath) as? PictureTableViewCell else {
            fatalError("PictureTableViewCell dequeue error")
        }
        return cell
    }

}
