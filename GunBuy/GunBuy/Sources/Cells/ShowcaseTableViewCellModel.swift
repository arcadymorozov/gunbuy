/**

 Площадка

 */

import UIKit

class ShowcaseTableViewCellModel {

    /**
     Установка значения ячейки Редактирования свойства.
     - parameter cell: Ячейка редактирования текстового свойства.
     - parameter showcase: Выставка
     - parameter indexPath: Адрес ячейки.
     */
    class func setup(_ cell: ShowcaseTableViewCell, with showcase: Showcase, at indexPath: IndexPath? = nil) {
        cell.indexPath = indexPath
        cell.nameLabel.text = showcase.name
        cell.detailsLabel.text = showcase.details
        if let count = showcase.suppliers?.count, count > 0 {
            cell.numberLabel.text = "\(count)"
        } else {
            cell.numberLabel.text = nil
        }
        if let style = showcase.showcaseStyle {
            switch style {
            case .exhibition:
                cell.iconView.backgroundColor = UIColor(hexString: "00A8F5")
            case .market:
                cell.iconView.backgroundColor = UIColor(hexString: "FFB020")
            case .factory:
                cell.iconView.backgroundColor = UIColor(hexString: "97CF26")
            case .wholesale:
                cell.iconView.backgroundColor = UIColor(hexString: "9E6CEF")
            default:
                cell.iconView.backgroundColor = UIColor.clear
            }
        }
    }

    /**
     Регистрирование ячейки Редактирования свойства в tableView.
     - parameter tableView: UITableView для которой регистрируется ячейка.
     */
    class func registerCell(for tableView: UITableView) {
        tableView.register(UINib(nibName: ShowcaseTableViewCell.identifier, bundle: nil), forCellReuseIdentifier: ShowcaseTableViewCell.identifier)
    }

    /**
     Извлечение ячейки Редактирования свойства в tableView.
     - parameter tableView: UITableView для которой извлекается ячейка.
     - parameter indexPath: Адрес ячейки.
     */
    class func cell(for tableView: UITableView, at indexPath: IndexPath) -> ShowcaseTableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ShowcaseTableViewCell.identifier, for: indexPath) as? ShowcaseTableViewCell else {
            fatalError("ShowcaseTableViewCell dequeue error")
        }
        return cell
    }

}
