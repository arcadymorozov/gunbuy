
import UIKit

class HeaderTableViewCell: UITableViewCell {

    static var identifier: String {
        return "HeaderTableViewCell"
    }

    @IBOutlet weak var titleLabel: UILabel!

    @IBOutlet weak var separatorViewHeight: NSLayoutConstraint! {
        didSet {
            separatorViewHeight.constant = 0.5
        }
    }

}
