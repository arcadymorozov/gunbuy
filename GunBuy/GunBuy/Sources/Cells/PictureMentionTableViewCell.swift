/**

 Медиа. Изображения.

 */

import UIKit

class PictureMentionTableViewCell: MentionTableViewCell {

    override class var identifier: String {
        return "PictureMentionTableViewCell"
    }

    @IBOutlet weak var pictureView: UIImageView! {
        didSet {
            pictureView.layer.cornerRadius = 5
            pictureView.layer.masksToBounds = true
            pictureView.layer.borderColor = UIColor.groupTableViewBackground.cgColor
            pictureView.layer.borderWidth = 1.0
        }
    }

    @IBOutlet fileprivate var sketchImageView: UIImageView!

    @IBOutlet fileprivate var canvasImageView: UIImageView!

    @IBOutlet weak var heightLayoutConstraint: NSLayoutConstraint!

    var lastPoint = CGPoint.zero

    var red: CGFloat = 255.0
    var green: CGFloat = 0.0
    var blue: CGFloat = 0.0

    var brushWidth: CGFloat = 4.0
    var brushOpacity: CGFloat = 1.0

    var swiped = false
}
/*
extension PictureMentionTableViewCell {

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        swiped = false
        if let touch = touches.first {
            lastPoint = touch.location(in: sketchImageView)
        }
    }

    func drawLine(fromPoint: CGPoint, toPoint: CGPoint) {
        UIGraphicsBeginImageContext(sketchImageView.frame.size)
        let context = UIGraphicsGetCurrentContext()
        canvasImageView.image?.draw(in: CGRect(x: 0, y: 0, width: sketchImageView.frame.size.width, height: sketchImageView.frame.size.height))

        context?.move(to: fromPoint)
        context?.addLine(to: toPoint)

        context?.setLineCap(.round)
        context?.setLineWidth(brushWidth)
        context?.setStrokeColor(red: red, green: green, blue: blue, alpha: 1.0)
        context?.setBlendMode(.normal)

        context?.strokePath()

        canvasImageView.image = UIGraphicsGetImageFromCurrentImageContext()
        canvasImageView.alpha = brushOpacity
        UIGraphicsEndImageContext()
    }

    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        swiped = true
        if let touch = touches.first {
            let currentPoint = touch.location(in: sketchImageView)
            drawLine(fromPoint: lastPoint, toPoint: currentPoint)

            lastPoint = currentPoint
        }
    }

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if !swiped {
            drawLine(fromPoint: lastPoint, toPoint: lastPoint)
        }

        UIGraphicsBeginImageContext(sketchImageView.frame.size)
        sketchImageView.image?.draw(in: CGRect(x: 0, y: 0, width: sketchImageView.frame.size.width, height: sketchImageView.frame.size.height), blendMode: .normal, alpha: 1.0)
        canvasImageView.image?.draw(in: CGRect(x: 0, y: 0, width: sketchImageView.frame.size.width, height: sketchImageView.frame.size.height), blendMode: .normal, alpha: brushOpacity)
        sketchImageView.image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        canvasImageView.image = nil
    }

}
*/
