
import UIKit

protocol PictureTableViewCellDelegate: class {

    /**
     Начало редактиврования тектсового поля

     - parameter cell: Ячейка таблицы, в которой начато редактирование textField
     - parameter textField: Текстовое поле, в котором начато редактирование
     - parameter indexPath: Текущий IndexPath для ячейки cell
     */
    func pictureTableViewCell(_ cell: PictureTableViewCell, didBeginEditing textView: UITextView, at indexPath: IndexPath?)

    /**
     Окончание редактирования тектсового поля

     - parameter cell: Ячейка таблицы, в которой окончено редактирование textView
     - parameter value: Значение текстового поля
     - parameter indexPath: Текущий IndexPath для ячейки cell
     */
    func pictureTableViewCell(_ cell: PictureTableViewCell, didChange value: String?, at indexPath: IndexPath?)

    /**
     Выбор фотографии

     - parameter cell: Ячейка таблицы, в которой окончено редактирование textView
     - parameter image: Фотография
     - parameter indexPath: Текущий IndexPath для ячейки cell
     */
    func pictureTableViewCell(_ cell: PictureTableViewCell, willSelect image: UIImage?, at indexPath: IndexPath?)


}

class PictureTableViewCell: UITableViewCell {

    static var identifier: String {
        return "PictureTableViewCell"
    }

    var indexPath: IndexPath?

    weak var delegate: PictureTableViewCellDelegate?

    @IBOutlet weak var textView: UITextView!

    @IBOutlet weak var pictureImageView: UIImageView! {
        didSet {
            pictureImageView.layer.cornerRadius = 5
            pictureImageView.layer.masksToBounds = true
            pictureImageView.layer.borderColor = UIColor.groupTableViewBackground.cgColor
            pictureImageView.layer.borderWidth = 1.0
        }
    }

    @IBOutlet weak var surfaceView: UIView! {
        didSet {
            surfaceView.layer.cornerRadius = 5
            surfaceView.layer.masksToBounds = true
            surfaceView.layer.borderColor = UIColor.groupTableViewBackground.cgColor
            surfaceView.layer.borderWidth = 1.0
        }
    }

    @IBAction func doSelectImage(_ sender: UIButton) {
        delegate?.pictureTableViewCell(self, willSelect: pictureImageView.image, at: indexPath)
    }

}

extension PictureTableViewCell: UITextViewDelegate {

    func textViewDidBeginEditing(_ textView: UITextView) {
        delegate?.pictureTableViewCell(self, didBeginEditing: textView, at: indexPath)
    }

    func textViewDidEndEditing(_ textView: UITextView) {
        delegate?.pictureTableViewCell(self, didChange: textView.text, at: indexPath)
    }

}
