/**

 Редактирование строки текста

 */

import UIKit

/**
 Тип тектсового поля
 */
enum TextFieldStyle {
    case text
    case email
    case number 
}

/**
 Отслеживание редактиврования тектсового поля
 */
protocol TextFieldTableViewCellDelegate: class {

    /**
     Начало редактиврования тектсового поля

     - parameter cell: Ячейка таблицы, в которой начато редактирование textField
     - parameter textField: Текстовое поле, в котором начато редактирование
     - parameter indexPath: Текущий IndexPath для ячейки cell
     */
    func textFieldTableViewCell(_ cell: TextFieldTableViewCell, didBeginEditing textField: UITextField, at indexPath: IndexPath?)

    /**
     Окончание редактирования тектсового поля

     - parameter cell: Ячейка таблицы, в которой окончено редактирование textField
     - parameter value: Значение текстового поля
     - parameter indexPath: Текущий IndexPath для ячейки cell
     */
    func textFieldTableViewCell(_ cell: TextFieldTableViewCell, didChange value: String?, at indexPath: IndexPath?)

}

class TextFieldTableViewCell: UITableViewCell {

    static var identifier: String {
        return "TextFieldTableViewCell"
    }

    weak var delegate: TextFieldTableViewCellDelegate?

    var indexPath: IndexPath?

    var style: TextFieldStyle = .text {
        didSet {
            switch style {
            case .text: valueTextField.keyboardType = .default
            case .email: valueTextField.keyboardType = .emailAddress
            case .number: valueTextField.keyboardType = .numbersAndPunctuation
            }
        }
    }

    @IBOutlet weak var surfaceView: UIView! {
        didSet {
            surfaceView.layer.cornerRadius = 5
            surfaceView.layer.masksToBounds = true
            surfaceView.layer.borderColor = UIColor.groupTableViewBackground.cgColor
            surfaceView.layer.borderWidth = 1.0
        }
    }

    @IBOutlet weak var valueTextField: UITextField!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func resignResponder() {
        valueTextField.resignFirstResponder()
    }

    func becomeResponder() {
        valueTextField.becomeFirstResponder()
    }

}

extension TextFieldTableViewCell: UITextFieldDelegate {

    func textFieldDidBeginEditing(_ textField: UITextField) {
        delegate?.textFieldTableViewCell(self, didBeginEditing: textField, at: indexPath)
    }

    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        delegate?.textFieldTableViewCell(self, didChange: textField.text, at: indexPath)
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

}
