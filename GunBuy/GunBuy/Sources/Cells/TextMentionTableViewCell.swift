/**

 Медиа. Текст.

 */

import UIKit

class TextMentionTableViewCell: MentionTableViewCell {

    override class var identifier: String {
        return "TextMentionTableViewCell"
    }

    @IBOutlet weak var titleLabel: UILabel!

    @IBOutlet weak var textView: UITextView! {
        didSet {
            textView.textContainerInset = .zero
            textView.textContainer.lineFragmentPadding = 0
        }
    }

    @IBOutlet weak var heightLayoutConstraint: NSLayoutConstraint!

    @IBOutlet weak var surfaceView: UIView! {
        didSet {
            surfaceView.layer.cornerRadius = 5
            surfaceView.layer.masksToBounds = true
            surfaceView.layer.borderColor = UIColor.groupTableViewBackground.cgColor
            surfaceView.layer.borderWidth = 1.0
        }
    }

    fileprivate var isScrollNeeded: Bool = false

    override func awakeFromNib() {
        super.awakeFromNib()
        heightLayoutConstraint.constant = 20.5

        let placeholderLabel = UILabel()
        placeholderLabel.text = "Add Text".localized
        placeholderLabel.font = UIFont.systemFont(ofSize: (textView.font?.pointSize)!)
        placeholderLabel.sizeToFit()
        textView.addSubview(placeholderLabel)
        placeholderLabel.frame.size = CGSize(width: textView.frame.size.width, height: placeholderLabel.frame.size.height)
        placeholderLabel.frame.origin = CGPoint(x: 3, y: 0)
        placeholderLabel.textColor = UIColor.lightGray
        placeholderLabel.alpha = 0.6
        titleLabel = placeholderLabel
    }

    func resignResponder() {
        textView.resignFirstResponder()
    }

    func becomeResponder() {
        textView.becomeFirstResponder()
    }

    func resize(_ textView: UITextView) {
        titleLabel.isHidden = !textView.text.isEmpty
        let fixedWidth = textView.frame.size.width
        //textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        let newSize = textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        var newFrame = textView.frame
        newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
        textView.frame = newFrame;
        heightLayoutConstraint.constant = newFrame.size.height + 1
    }

}

extension TextMentionTableViewCell: UITextViewDelegate {

    func textViewDidBeginEditing(_ textView: UITextView) {
        //delegate?.textViewTableViewCell(self, didBeginEditing: textView, at: indexPath)
        delegate?.mentionTableViewCell(self, didBeginEditing: textView, at: indexPath)
    }

    func textViewDidEndEditing(_ textView: UITextView) {
        //delegate?.textViewTableViewCell(self, didChange: textView.text, at: indexPath)
        delegate?.mentionTableViewCell(self, didChange: textView.text as AnyObject?, at: indexPath)
    }

    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            isScrollNeeded = true
        }
        return true
    }

    func textViewDidChange(_ textView: UITextView) {
        resize(textView)
        tableView?.beginUpdates()
        tableView?.endUpdates()
        if let indexPath = indexPath, isScrollNeeded == true {
            isScrollNeeded = false
            tableView?.scrollToRow(at: indexPath, at: .bottom, animated: true)
        }
    }

}
