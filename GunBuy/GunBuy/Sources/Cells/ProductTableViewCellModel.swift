/**

 Товар. Модель.

 */

import UIKit

class ProductTableViewCellModel {

    /**
     Установка значения ячейки Редактирования свойства.
     - parameter cell: Ячейка редактирования текстового свойства.
     - parameter product: Название..
     - parameter indexPath: Адрес ячейки.
     */
    class func setup(_ cell: ProductTableViewCell, with product: Product, at indexPath: IndexPath? = nil) {
        cell.indexPath = indexPath
        cell.nameLabel.text = product.name
        cell.costLabel.text = "\(product.cost ?? "") \(product.currency ?? "")"
        cell.quantityLabel.text = product.quantity
        cell.iconImageView.image = product.picture
    }

    /**
     Регистрирование ячейки Редактирования свойства в tableView.
     - parameter tableView: UITableView для которой регистрируется ячейка.
     */
    class func registerCell(for tableView: UITableView) {
        tableView.register(UINib(nibName: ProductTableViewCell.identifier, bundle: nil), forCellReuseIdentifier: ProductTableViewCell.identifier)
    }

    /**
     Извлечение ячейки Редактирования свойства в tableView.
     - parameter tableView: UITableView для которой извлекается ячейка.
     - parameter indexPath: Адрес ячейки.
     */
    class func cell(for tableView: UITableView, at indexPath: IndexPath) -> ProductTableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ProductTableViewCell.identifier, for: indexPath) as? ProductTableViewCell else {
            fatalError("ProductTableViewCell dequeue error")
        }
        return cell
    }

}
