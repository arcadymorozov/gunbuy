/**

Медиа. Свойство.

*/

import UIKit

class PropertyMentionTableViewCell: MentionTableViewCell {

    override class var identifier: String {
        return "PropertyMentionTableViewCell"
    }

    @IBOutlet weak var surfaceView: UIView! {
        didSet {
            surfaceView.layer.cornerRadius = 5
            surfaceView.layer.masksToBounds = true
            surfaceView.layer.borderColor = UIColor.groupTableViewBackground.cgColor
            surfaceView.layer.borderWidth = 1.0
        }
    }

    @IBOutlet weak var valueTextField: UITextField! {
        didSet {
            valueTextField.placeholder = "Enter value".localized
        }
    }

    @IBOutlet weak var titleLabel: UILabel!

}

extension PropertyMentionTableViewCell: UITextFieldDelegate {

    func textFieldDidBeginEditing(_ textField: UITextField) {
        delegate?.mentionTableViewCell(self, didBeginEditing: textField, at: indexPath)
    }

    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        delegate?.mentionTableViewCell(self, didChange: textField.text as AnyObject?, at: indexPath)
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

}
