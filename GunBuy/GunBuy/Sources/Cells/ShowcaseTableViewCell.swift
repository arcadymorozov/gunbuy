/**

 Витрина, выставка и т.д.

 */

import UIKit

class ShowcaseTableViewCell: UITableViewCell {

    static var identifier: String {
        return "ShowcaseTableViewCell"
    }

    var indexPath: IndexPath?

    @IBOutlet weak var iconView: UIView! {
        didSet {
            iconView.layer.cornerRadius = 5;
            iconView.layer.masksToBounds = true;
        }
    }

    @IBOutlet weak var nameLabel: UILabel!

    @IBOutlet weak var numberLabel: UILabel!

    @IBOutlet weak var detailsLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        accessoryView = AcessoryIconView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
    }

}
