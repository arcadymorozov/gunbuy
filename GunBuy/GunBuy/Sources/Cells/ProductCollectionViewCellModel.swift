/**

 Товар
 Ячейка коллекции

 */

import UIKit

final class ProductCollectionViewCellModel {

    /**
     Установка значения ячейки Редактирования свойства.
     - parameter cell: Ячейка редактирования текстового свойства.
     - parameter product: Название..
     - parameter indexPath: Адрес ячейки.
     */
    class func setup(_ cell: ProductCollectionViewCell, with product: Product, at indexPath: IndexPath? = nil) {
        cell.indexPath = indexPath
        cell.nameLabel.text = product.name
        cell.iconImageView.image = product.picture
    }

    /**
     Регистрирование ячейки Редактирования свойства в tableView.
     - parameter collectionView: UICollectionView для которой регистрируется ячейка.
     */
    class func registerCell(for collectionView: UICollectionView) {
        collectionView.register(UINib(nibName: ProductCollectionViewCell.identifier, bundle: nil), forCellWithReuseIdentifier: ProductCollectionViewCell.identifier)
    }

    /**
     Извлечение ячейки Редактирования свойства в tableView.
     - parameter collectionView: UICollectionView для которой извлекается ячейка.
     - parameter indexPath: Адрес ячейки.
     */
    class func cell(for collectionView: UICollectionView, at indexPath: IndexPath) -> ProductCollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProductCollectionViewCell.identifier, for: indexPath) as? ProductCollectionViewCell else {
            fatalError("ProductCollectionViewCell dequeue error")
        }
        return cell
    }

}
