/**

 Товар.

 */

import UIKit

class ProductTableViewCell: UITableViewCell {

    static var identifier: String {
        return "ProductTableViewCell"
    }

    var indexPath: IndexPath?

    @IBOutlet weak var nameLabel: UILabel!

    @IBOutlet weak var costLabel: UILabel!

    @IBOutlet weak var quantityLabel: UILabel!

    @IBOutlet weak var detailsLabel: UILabel!

    @IBOutlet weak var iconImageView: UIImageView! {
        didSet {
            iconImageView.layer.cornerRadius = 5
            iconImageView.layer.masksToBounds = true
            iconImageView.layer.borderColor = UIColor.groupTableViewBackground.cgColor
            iconImageView.layer.borderWidth = 1.0
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        accessoryView = AcessoryIconView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
    }

}
