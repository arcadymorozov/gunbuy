/**

 Редактирование цены

 */

import UIKit

class PriceTableViewCellModel {

    /**
     Установка значения ячейки Редактирования свойства.
     - parameter cell: Ячейка редактирования текстового свойства.
     - parameter title: Название свойства.
     - parameter value: Значения текстового свойства.
     - parameter style: Стиль текстового поля TextFieldStyle. Поумолчанию Текст.
     - parameter delegate: Получатель событий TextFieldTableViewCellDelegate.
     - parameter indexPath: Адрес ячейки.
     */
    class func setup(_ cell: PriceTableViewCell, title: String, cost: String?, currency: String?, delegate: PriceTableViewCellDelegate? = nil, at indexPath: IndexPath? = nil) {
        cell.delegate = delegate
        cell.indexPath = indexPath
        cell.costTextField.placeholder = title
        cell.costTextField.text = cost
        cell.currencyTextField.text = currency

    }

    /**
     Регистрирование ячейки Редактирования свойства в tableView.
     - parameter tableView: UITableView для которой регистрируется ячейка.
     */
    class func registerCell(for tableView: UITableView) {
        tableView.register(UINib(nibName: PriceTableViewCell.identifier, bundle: nil), forCellReuseIdentifier: PriceTableViewCell.identifier)
    }

    /**
     Извлечение ячейки Редактирования свойства в tableView.
     - parameter tableView: UITableView для которой извлекается ячейка.
     - parameter indexPath: Адрес ячейки.
     */
    class func cell(for tableView: UITableView, at indexPath: IndexPath) -> PriceTableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: PriceTableViewCell.identifier, for: indexPath) as? PriceTableViewCell else {
            fatalError("PriceTableViewCell dequeue error")
        }
        return cell
    }

}
