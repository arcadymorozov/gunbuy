/**

 Медиа контент

 */

import UIKit

class MentionTableViewCellProvider {

    /**
     Регистрирование ячейки Редактирования свойства в tableView.
     - parameter tableView: UITableView для которой регистрируется ячейка.
     */
    class func registerCells(for tableView: UITableView) {
        tableView.register(UINib(nibName: MentionTableViewCell.identifier, bundle: nil), forCellReuseIdentifier: MentionTableViewCell.identifier)
        tableView.register(UINib(nibName: TextMentionTableViewCell.identifier, bundle: nil), forCellReuseIdentifier: TextMentionTableViewCell.identifier)
        tableView.register(UINib(nibName: PictureMentionTableViewCell.identifier, bundle: nil), forCellReuseIdentifier: PictureMentionTableViewCell.identifier)
        tableView.register(UINib(nibName: PropertyMentionTableViewCell.identifier, bundle: nil), forCellReuseIdentifier: PropertyMentionTableViewCell.identifier)
    }

    /**
     Извлечение ячейки Редактирования свойства в tableView.
     - parameter tableView: UITableView для которой извлекается ячейка.
     - parameter indexPath: Адрес ячейки.
     */
    class func cell(for tableView: UITableView, with mention: Mention?, delegate: MentionTableViewCellDelegate? = nil, at indexPath: IndexPath) -> MentionTableViewCell {
        switch mention?.mentionStyle {

        case .text:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: TextMentionTableViewCell.identifier, for: indexPath) as? TextMentionTableViewCell else {
                fatalError("TextMentionTableViewCell dequeue error")
            }
            cell.delegate = delegate
            cell.tableView = tableView
            cell.indexPath = indexPath
            if let text = mention?.data as? String {
                cell.textView.text = text
            } else {
                cell.textView.text = nil
            }
            cell.resize(cell.textView)
            return cell

        case .image:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: PictureMentionTableViewCell.identifier, for: indexPath) as? PictureMentionTableViewCell else {
                fatalError("PictureMentionTableViewCell dequeue error")
            }
            cell.delegate = delegate
            cell.tableView = tableView
            cell.indexPath = indexPath
            if let image = mention?.data as? UIImage {
                cell.pictureView.image = image
                cell.heightLayoutConstraint.constant = cell.pictureView.frame.size.width * image.size.height / image.size.width
            } else {
                cell.pictureView.image = nil
            }
            return cell

        case .property:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: PropertyMentionTableViewCell.identifier, for: indexPath) as? PropertyMentionTableViewCell else {
                fatalError("PropertyMentionTableViewCell dequeue error")
            }
            cell.delegate = delegate
            cell.tableView = tableView
            cell.indexPath = indexPath
            if let data = mention?.data as? [String: String] {
                cell.valueTextField.text = data["value"]
                cell.titleLabel.text = data["title"]?.localized
            } else {
                cell.valueTextField.text = nil
                cell.titleLabel.text = nil
            }
            return cell

        default:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: MentionTableViewCell.identifier, for: indexPath) as? MentionTableViewCell else {
                fatalError("MentionTableViewCell dequeue error")
            }
            cell.delegate = delegate
            cell.tableView = tableView
            cell.indexPath = indexPath
            return cell
        }
    }

}
