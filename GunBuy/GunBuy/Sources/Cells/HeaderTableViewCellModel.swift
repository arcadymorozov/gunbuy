import UIKit

class HeaderTableViewCellModel {

    /**
     Установка значения ячейки Редактирования свойства.
     */
    class func setup(_ cell: HeaderTableViewCell, with title: String?) {
        cell.titleLabel.text = title
    }

    /**
     Регистрирование ячейки Редактирования свойства в tableView.
     - parameter tableView: UITableView для которой регистрируется ячейка.
     */
    class func registerCell(for tableView: UITableView) {
        tableView.register(UINib(nibName: HeaderTableViewCell.identifier, bundle: nil), forCellReuseIdentifier: HeaderTableViewCell.identifier)
    }

    /**
     Извлечение ячейки Редактирования свойства в tableView.
     - parameter tableView: UITableView для которой извлекается ячейка.
     - parameter indexPath: Адрес ячейки.
     */
    class func cell(for tableView: UITableView, at indexPath: IndexPath) -> HeaderTableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: HeaderTableViewCell.identifier, for: indexPath) as? HeaderTableViewCell else {
            fatalError("HeaderTableViewCell dequeue error")
        }
        return cell
    }

}
