
import UIKit

final class AppRouter: NSObject {

    fileprivate var mainWindow: UIWindow?
    fileprivate var dataController: CoreDataController!

    /**
     Инициализация контроллеров. При старте установлен
     MainViewController (LaunchScreen)
     */
    init(with window: UIWindow?) {
        self.mainWindow = window
        self.dataController = CoreDataController()
    }

    /** Вызов при переходе приложения в активное состояние */
    func becomeActive() {
        print("........becomeActive")
    }

    /**
     Вызов при уходе приложения в фон
     */
    func resignActive() {
        print("........resignActive")
    }

    /**
     Запуск
     */
    func start() {
        self.mainWindow?.rootViewController = UIStoryboard.Showcases.main(dataController: dataController).viewController
    }

    /**
     Отображаемый в текущий момент контроллер
     */
    class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabBarController = controller as? UITabBarController {
            if let selected = tabBarController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }

    /**
     Отображение модального контроллера
     - parameter viewController: Контроллер для отображения.
     - parameter animated: Использование анимации при отображении. True - показать с использованием анимации.
     - parameter completion: Код, выполняемый после отображения контроллера.
     */
    func present(_ viewController: UIViewController, animated: Bool = false, completion: (() -> Void)? = nil) {
        AppRouter.topViewController()?.present(viewController, animated: animated) {
            completion?()
        }
    }

    /**
     Скрытие модального контроллера
     - parameter animated: Использование анимации при скрытии. True - скрыть с использованием анимации.
     - parameter completion: Код, выполняемый после отображения контроллера.
     */
    func dissmiss(animated: Bool = false, completion: (() -> Void)? = nil) {
        AppRouter.topViewController()?.dismiss(animated: animated) {
            completion?()
        }
    }

}
