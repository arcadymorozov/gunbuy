/**

 Визуальное оформление элементов.
 Вызывается из AppDelegate.didFinishLaunchingWithOptions

 */

import UIKit

struct AppearanceController {

    static func setupAppearance() {
        setupModule()
    }

    private static func setupModule() {
        let attrs = [
            //NSAttributedString.Key.foregroundColor: UIColor.red,
            NSAttributedString.Key.font: UIFont.systemFont(ofSize: 21)
        ]

        UINavigationBar.appearance().titleTextAttributes = attrs
    }

}
