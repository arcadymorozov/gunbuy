
import UIKit

enum Property: String {
    case ctn // (количестов ящиков)
    case qty // (количество в коробоке)
    case cbn // (объем коробоки в м3)
    case number // (номер который указал байер)
    case unit // (единица измерения)

    var title: String {
        return self.rawValue.localized
    }
}

class PropertiesDataContext {

    let properties: [Property] = [.ctn, .qty, .cbn, .number, .unit]

}

extension PropertiesDataContext {

    func registerCells(for tableView: UITableView) {
    }

    func numberOfSections(for tableView: UITableView) -> Int {
        return 1
    }

    func numberOfRows(for tableView: UITableView, in section: Int) -> Int {
        return properties.count
    }

    func dequeueReusableCell(for tableView: UITableView, in viewController: UIViewController?, at indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DefaultCell", for: indexPath)
        cell.textLabel?.text = properties[indexPath.row].title
        return cell
    }

}
