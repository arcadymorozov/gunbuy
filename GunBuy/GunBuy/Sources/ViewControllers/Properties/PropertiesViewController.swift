/**

 Свойства

 */

import UIKit

/**
 Оповещение об изменениях..
 */
protocol PropertiesViewControllerDelegate: class {

    func propertiesViewController(_ propertiesViewController: PropertiesViewController, willSelect property: Property?)

}

class PropertiesViewController: UIViewController {

    weak var delegate: PropertiesViewControllerDelegate?

    @IBOutlet fileprivate var tableView: UITableView! {
        didSet {
            tableView.rowHeight = UITableView.automaticDimension
            tableView.estimatedRowHeight = 70.0
            tableView.tableFooterView = UIView()
            tableView.contentInset = UIEdgeInsets(top: 12, left: 0, bottom: 100, right: 0)
            // Register NIB
            context.registerCells(for: tableView)
        }
    }

    lazy var context: PropertiesDataContext = PropertiesDataContext()

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Property".localized
    }

}

// MARK: - Table Source

extension PropertiesViewController: UITableViewDelegate, UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return context.numberOfSections(for: tableView)
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return context.numberOfRows(for: tableView, in: section)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return context.dequeueReusableCell(for: tableView, in: self, at: indexPath)
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.propertiesViewController(self, willSelect: context.properties[indexPath.row])
    }

}
