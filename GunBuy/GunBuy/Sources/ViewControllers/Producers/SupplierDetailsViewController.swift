/**

 Производитель

 */

import UIKit
import CoreData

class SupplierDetailsViewController: UIViewController {

    @IBOutlet fileprivate var tableView: UITableView! {
        didSet {
            tableView.rowHeight = UITableView.automaticDimension
            tableView.estimatedRowHeight = 70.0
            tableView.tableFooterView = UIView()
            tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 100, right: 0)
            // Register NIB
            context.registerCells(for: tableView)
        }
    }

    @IBOutlet fileprivate var addButton: UIButton! {
        didSet {
            addButton.setBackgroundImage(UIImage(style: .roundedAdd, frame: CGRect(x: 0, y: 0, width: 480, height: 480)), for: .normal)
        }
    }

    @IBOutlet fileprivate var addLabel: UILabel! {
        didSet {
            addLabel.text = "New Product".localized
        }
    }

    @IBOutlet fileprivate var collectionView: UICollectionView! {
        didSet {
            // Register NIB
            context.registerCells(for: collectionView)
        }
    }

    @IBOutlet fileprivate var imageView: UIImageView!

    @IBOutlet fileprivate var nameLabel: UILabel!

    var context: SupplierDetailsDataContext!

    private struct SearchControllerRestorableState {
        var wasActive = false
        var wasFirstResponder = false
    }

    /// Search controller to help us with filtering.
    private var searchController: UISearchController!

    /// Restoration state for UISearchController
    private var restoredState = SearchControllerRestorableState()

    override func viewDidLoad() {
        super.viewDidLoad()

        addSearchController()
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(doEdit))
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(contextDidChange), name: SupplierDetailsDataContextDidChangeSuccessfuly, object: nil)
        fetchData()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // Restore the searchController's active state.
        if restoredState.wasActive {
            searchController.isActive = restoredState.wasActive
            restoredState.wasActive = false

            if restoredState.wasFirstResponder {
                searchController.searchBar.becomeFirstResponder()
                restoredState.wasFirstResponder = false
            }
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }

    func addSearchController() {
        //MARK: - Search Controller
        let searchResultsController = SearchResultsController(context: context, presenter: self)
        searchController = UISearchController(searchResultsController: searchResultsController)
        searchController.searchResultsUpdater = self
        searchController.searchBar.autocapitalizationType = .none
        if #available(iOS 11.0, *) {
            navigationItem.searchController = searchController
            navigationItem.hidesSearchBarWhenScrolling = true
        } else {
            tableView.tableHeaderView = searchController.searchBar
        }
        //searchController.delegate = self
        searchController.dimsBackgroundDuringPresentation = true // The default is true.
        searchController.searchBar.delegate = self // Monitor when the search button is tapped.
        definesPresentationContext = true
    }

    /** Извлечение данных в контексте */
    func fetchData() {
        context.fetchData()
        reloadData()
    }

    /** Обновление данных */
    func reloadData() {
        title = context.title
        imageView.image = context.supplier?.picture
        //nameLabel.text = context.producer?.name
        tableView.reloadData()
        tableView.isHidden = context.isEmpty

        //collectionView.reloadData()
    }

    @IBAction func doAdd(_ sender: UIBarButtonItem) {
        let shootViewController = UIStoryboard.Products.shoot(delegate: self).viewController
        present(shootViewController, animated: true)
    }

    @objc func contextDidChange(_ notification: Notification) {
        fetchData()
    }

    /**
     Редактирования производителя
     */
    @IBAction func doEdit(_ sender: UIBarButtonItem) {
        context.modifySupplier(presenter: self)
    }

}

// MARK: - Table Source

extension SupplierDetailsViewController: UITableViewDelegate, UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return context.numberOfSections(for: tableView)
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return context.numberOfRows(for: tableView, in: section)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return context.dequeueReusableCell(for: tableView, in: self, at: indexPath)
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        context.didSelect(for: tableView, in: self, at: indexPath)
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return context.titleForHeader(for: tableView, in: section)
    }

    // Edit Rows

    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            // handle delete (by removing the data from your array and updating the tableview)
            let alertController = UIAlertController(title: nil, message: "Remove Product?".localized, preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Yes".localized, style: .default, handler: { [weak self] (_) in
                self?.context.removeProduct(at: indexPath)
            }))
            alertController.addAction(UIAlertAction(title: "No".localized, style: .cancel))
            present(alertController, animated: true)
        }
    }

}

// MARK: - Collection View Source

extension SupplierDetailsViewController: UICollectionViewDataSource, UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return context.numberOfItems(for: collectionView, in: section)
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return context.dequeueReusableCell(for: collectionView, in: self, at: indexPath)
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        context.didSelect(for: collectionView, in: self, at: indexPath)
    }
}

/**
 Создание продукта. Сделано Фото.
 */
extension SupplierDetailsViewController: CaptureMediaViewControllerDelegate {

    func captureMediaViewController(_ captureMediaViewController: CaptureMediaViewController?, didFinishWithImage image: UIImage?, preview: UIImage?) {
        context.createProduct(image: image, preview: preview, presenter: captureMediaViewController)
    }

    func captureMediaViewControllerWillCancel(_ captureMediaViewController: CaptureMediaViewController) {
        dismiss(animated: true)
    }

}

//MARK: - Редактирование Showcase -

extension SupplierDetailsViewController: SupplierModifyViewControllerDelegate {

    func supplierModifyViewController(_ supplierModifyViewController: SupplierModifyViewController, didChange supplier: Supplier?, in dataContext: NSManagedObjectContext) {
        context.dataController.save(changesIn: dataContext)
        reloadData()
        dismiss(animated: true)
    }

    func supplierModifyViewControllerWillCancel(_ supplierModifyViewController: SupplierModifyViewController) {
        dismiss(animated: true)
    }

}

// MARK: - SearchController

extension SupplierDetailsViewController: UISearchBarDelegate {

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }

}

extension SupplierDetailsViewController: UISearchResultsUpdating {

    func updateSearchResults(for searchController: UISearchController) {
        context.updateSearchResults(with: searchController.searchBar.text!, in: searchController.searchResultsController!)
    }

}
