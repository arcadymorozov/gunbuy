/**

 Список производителей с выставки

*/

import UIKit
import CoreData

class SuppliersViewController: UIViewController {

    @IBOutlet fileprivate var tableView: UITableView! {
        didSet {
            tableView.rowHeight = UITableView.automaticDimension
            tableView.estimatedRowHeight = 70.0
            tableView.tableFooterView = UIView()
            tableView.contentInset = UIEdgeInsets(top: 12, left: 0, bottom: 100, right: 0)
            // Register NIB
            context.registerCells(for: tableView)
        }
    }

    @IBOutlet fileprivate var titleLabel: UILabel! {
        didSet {
            titleLabel.text = "No Suppliers".localized
        }
    }

    @IBOutlet fileprivate var subtitleLabel: UILabel! {
        didSet {
            subtitleLabel.text = "Add Supplier...".localized
        }
    }

    @IBOutlet fileprivate weak var sorrowImageView: UIImageView! {
        didSet {
            sorrowImageView.image = UIImage(style: .sorrow, frame: CGRect(x: 0, y: 0, width: 160, height: 160))
        }
    }

    @IBOutlet fileprivate var addButton: UIButton! {
        didSet {
            addButton.setBackgroundImage(UIImage(style: .roundedAdd, frame: CGRect(x: 0, y: 0, width: 480, height: 480)), for: .normal)
        }
    }

    @IBOutlet fileprivate var addLabel: UILabel! {
        didSet {
            addLabel.text = "New Supplier".localized
        }
    }

    /** Контекст данных */
    var context: SuppliersDataContext!

    private struct SearchControllerRestorableState {
        var wasActive = false
        var wasFirstResponder = false
    }

    /// Search controller to help us with filtering.
    private var searchController: UISearchController!

    /// Restoration state for UISearchController
    private var restoredState = SearchControllerRestorableState()

    override func viewDidLoad() {
        super.viewDidLoad()

        addSearchController()
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(doEdit))
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(contextDidChange), name: SuppliersDataContextDidChangeSuccessfuly, object: nil)
        fetchData()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // Restore the searchController's active state.
        if restoredState.wasActive {
            searchController.isActive = restoredState.wasActive
            restoredState.wasActive = false

            if restoredState.wasFirstResponder {
                searchController.searchBar.becomeFirstResponder()
                restoredState.wasFirstResponder = false
            }
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }

    func addSearchController() {
        //MARK: - Search Controller
        let searchResultsController = SearchResultsController(context: context, presenter: self)
        searchController = UISearchController(searchResultsController: searchResultsController)
        searchController.searchResultsUpdater = self
        searchController.searchBar.autocapitalizationType = .none
        if #available(iOS 11.0, *) {
            navigationItem.searchController = searchController
            navigationItem.hidesSearchBarWhenScrolling = true
        } else {
            tableView.tableHeaderView = searchController.searchBar
        }
        //searchController.delegate = self
        searchController.dimsBackgroundDuringPresentation = true // The default is true.
        searchController.searchBar.delegate = self // Monitor when the search button is tapped.
        definesPresentationContext = true
    }

    /** Извлечение данных в контексте */
    func fetchData() {
        context.fetchData()
        reloadData()
    }

    /** Обновление данных */
    func reloadData() {
        title = context.title
        tableView.reloadData()
        tableView.isHidden = context.isEmpty
    }

    @objc func contextDidChange(_ notification: Notification) {
        fetchData()
    }

    /**
     Добавление поставщика
     */
    @IBAction func doAdd(_ sender: UIBarButtonItem) {
        let shootViewController = UIStoryboard.Suppliers.shoot(delegate: self).viewController
        present(shootViewController, animated: true)
    }

    /**
     Редактирования мероприятия
     */
    @IBAction func doEdit(_ sender: UIBarButtonItem) {
        context.modifyShowcase(presenter: self)
    }

}

// MARK: - Table Source

extension SuppliersViewController: UITableViewDelegate, UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return context.numberOfSections(for: tableView)
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return context.numberOfRows(for: tableView, in: section)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return context.dequeueReusableCell(for: tableView, in: self, at: indexPath)
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        context.didSelect(for: tableView, in: self, at: indexPath)
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return context.titleForHeader(for: tableView, in: section)
    }


    // Edit Rows

    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            // handle delete (by removing the data from your array and updating the tableview)
            let alertController = UIAlertController(title: nil, message: "Remove Supplier?".localized, preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Yes".localized, style: .default, handler: { [weak self] (_) in
                self?.context.removeSupplier(at: indexPath)
            }))
            alertController.addAction(UIAlertAction(title: "No".localized, style: .cancel))
            present(alertController, animated: true)
        }
    }

}

extension SuppliersViewController: CaptureMediaViewControllerDelegate {

    func captureMediaViewController(_ captureMediaViewController: CaptureMediaViewController?, didFinishWithImage image: UIImage?, preview: UIImage?) {
        context.createSupplier(image: image, preview: preview, presenter: captureMediaViewController)
    }

    func captureMediaViewControllerWillCancel(_ capturePhotoViewController: CaptureMediaViewController) {
        dismiss(animated: true)
    }

}

// MARK: - SearchController

extension SuppliersViewController: UISearchBarDelegate {

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }

}

extension SuppliersViewController: UISearchResultsUpdating {

    func updateSearchResults(for searchController: UISearchController) {
        context.updateSearchResults(with: searchController.searchBar.text!, in: searchController.searchResultsController!)
    }

}

//MARK: - Редактирование Showcase -

extension SuppliersViewController: ShowcaseModifyViewControllerDelegate {

    /** Витрина успешно создана */
    func showcaseModifyViewController(_ showcaseModifyViewController: ShowcaseModifyViewController, didChange showcase: Showcase?, in dataContext: NSManagedObjectContext) {
        context.dataController.save(changesIn: dataContext)
        dismiss(animated: true)
    }

    func showcaseModifyViewControllerDidCancel(_ showcaseModifyViewController: ShowcaseModifyViewController) {
        dismiss(animated: true)
    }

}
