/**

 Снимок для производителя

 */

import UIKit

class SupplierCapturePhotoViewController: CaptureMediaViewController {

    @IBOutlet fileprivate weak var titleLabel: UILabel!

    @IBOutlet fileprivate weak var subtitleLabel: UILabel!

    @IBOutlet fileprivate var cutawayView: UIView! {
        didSet {
            cutawayView.layer.borderWidth = 1.5
            cutawayView.layer.cornerRadius = 5
            cutawayView.layer.borderColor = UIColor.white.cgColor
        }
    }

}
