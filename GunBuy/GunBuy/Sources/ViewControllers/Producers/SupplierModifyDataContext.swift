/**

 Редактирование производителя

 */

import UIKit
import CoreData

class SupplierModifyDataContext {

    var image: UIImage?

    var supplier: Supplier?

    let dataContext: NSManagedObjectContext!

    var sketchCoordinator: SketchCoordinator?

    var title: String {
        return supplier?.name ?? supplier?.showcase?.name ?? "Supplier".localized
    }

    enum Row {
        case name
        case picture
        case mention(Mention)

        var title: String {
            switch self {
            case .name: return "Name".localized
            case .picture: return "Picture".localized
            case .mention: return "Mention".localized
            }
        }
    }

    var rows: [Row] {
        var rows: [Row] = [.name, .picture]
        supplier?.mentions?.forEach({ (mention) in
            rows.append(.mention(mention as! Mention))
        })
        return rows
    }

    /** Создать контекст для работы с витриной */
    init(with supplier: Supplier?, in dataContext: NSManagedObjectContext, image: UIImage?) {
        self.supplier = supplier
        self.dataContext = dataContext
        self.image = image
    }

    var activeResponder: UIResponder?

    /** Убираем клавиатуру */
    func resignResponder() {
        activeResponder?.resignFirstResponder()
        activeResponder = nil
    }

    func insertMention(_ style: MentionStyle) -> Mention {
        let mention = Mention.create(in: dataContext)
        mention.style = Int16(style.rawValue)
        supplier?.addToMentions(mention)
        return mention
    }

    func removeMention(at indexPath: IndexPath) {
        let index = indexPath.row - 2
        supplier?.removeFromMentions(at: index)
    }

    /** Валидация данных */
    func validate(_ completion: (String) -> Void) -> Bool {
        guard let name = supplier?.name, !name.isEmpty else {
            completion("Enter Name".localized)
            return false
        }
        return true
    }

}

extension SupplierModifyDataContext: TableDataCollection {

    func registerCells(for tableView: UITableView) {
        PictureTableViewCellModel.registerCell(for: tableView)
        TextFieldTableViewCellModel.registerCell(for: tableView)
        MentionTableViewCellProvider.registerCells(for: tableView)
    }

    func numberOfSections(for tableView: UITableView) -> Int {
        return 1
    }

    func numberOfRows(for tableView: UITableView, in section: Int) -> Int {
        return rows.count
    }

    func dequeueReusableCell(for tableView: UITableView, in viewController: UIViewController?, at indexPath: IndexPath) -> UITableViewCell {
        switch rows[indexPath.row] {
        case .name:
            let cell = TextFieldTableViewCellModel.cell(for: tableView, at: indexPath)
            TextFieldTableViewCellModel.setup(cell, title: rows[indexPath.row].title, value: supplier?.name, style: .text, delegate: self, at: indexPath)
            return cell
        case .picture:
            let cell = PictureTableViewCellModel.cell(for: tableView, at: indexPath)
            PictureTableViewCellModel.setup(cell, with: supplier?.picture, text: supplier?.details, delegate: self)
            return cell
        case .mention(let mention):
            let cell = MentionTableViewCellProvider.cell(for: tableView, with: mention, delegate: self, at: indexPath)
            return cell
        }
    }

    /**
     Выбор пункта меню.
     */
    func didSelect(for tableView: UITableView, in viewController: UIViewController?, at indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let row = rows[indexPath.row]
        switch row {
        case .mention(let mention):
            if mention.mentionStyle == .image {
                sketchCoordinator = SketchCoordinator.instantiate(with: mention.data as? UIImage, in: viewController) { [weak self] (image) in
                    if image != nil {
                        mention.data = image
                        if let tableViewController = viewController as? UITableViewController {
                            tableViewController.tableView.reloadData()
                        }
                    }
                    viewController?.dismiss(animated: true)
                    self?.sketchCoordinator = nil
                }
            }
        default:
            break
        }
    }

    func canEditRow(for tableView: UITableView, at indexPath: IndexPath) -> Bool {
        if indexPath.row > 1 {
            return true
        }
        return false
    }

}

extension SupplierModifyDataContext: PictureTableViewCellDelegate {

    func pictureTableViewCell(_ cell: PictureTableViewCell, willSelect image: UIImage?, at indexPath: IndexPath?) {
        let viewController = AppRouter.topViewController()
        sketchCoordinator = SketchCoordinator.instantiate(with: supplier?.picture, in: viewController) { [weak self] (image) in
            if image != nil {
                self?.supplier?.picture = image
            }
            viewController?.dismiss(animated: true)
            self?.sketchCoordinator = nil
        }
    }

    func pictureTableViewCell(_ cell: PictureTableViewCell, didBeginEditing textView: UITextView, at indexPath: IndexPath?) {
        activeResponder = textView
    }

    func pictureTableViewCell(_ cell: PictureTableViewCell, didChange value: String?, at indexPath: IndexPath?) {
        supplier?.details = value
    }

}

extension SupplierModifyDataContext: TextFieldTableViewCellDelegate {

    func textFieldTableViewCell(_ cell: TextFieldTableViewCell, didBeginEditing textField: UITextField, at indexPath: IndexPath?) {
        activeResponder = textField
    }

    func textFieldTableViewCell(_ cell: TextFieldTableViewCell, didChange value: String?, at indexPath: IndexPath?) {
        activeResponder = nil
        if let indexPath = indexPath {
            switch rows[indexPath.row] {
            case .name: supplier?.name = value
            default: break
            }
        }
    }

}

extension SupplierModifyDataContext: MentionTableViewCellDelegate {

    func mentionTableViewCell(_ cell: MentionTableViewCell, didBeginEditing responder: UIResponder, at indexPath: IndexPath?) {
        activeResponder = responder
    }

    func mentionTableViewCell(_ cell: MentionTableViewCell, didChange value: AnyObject?, at indexPath: IndexPath?) {
        activeResponder = nil
        if let indexPath = indexPath {
            let row = rows[indexPath.row]
            switch row {
            case .mention(let mention):
                mention.data = value
            default:
                break
            }
        }
    }

}
