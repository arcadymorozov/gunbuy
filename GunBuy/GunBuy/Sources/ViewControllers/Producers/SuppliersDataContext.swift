/**

 Производители

 */

import UIKit
import CoreData

let SuppliersDataContextDidChangeSuccessfuly = Notification.Name(rawValue: "SuppliersDataContextDidChangeSuccessfuly")

class SuppliersDataContext: NSObject {

    var showcase: Showcase?

    /** Контроллер для извлечения данных */
    var fetchedResultsController: NSFetchedResultsController<Supplier>!

    /** Контроллер доступа к базе */
    var dataController: CoreDataController!

    var title: String {
        return showcase?.name ?? "Suppliers".localized
    }

    var isEmpty: Bool {
        return fetchedResultsController.fetchedObjects!.isEmpty
    }

    init(with showcase: Showcase?, dataController: CoreDataController) {
        self.showcase = showcase
        self.dataController = dataController
        self.fetchedResultsController = Supplier.fetchedResultsController(with: dataController.viewContext, showcase: showcase)
    }

    /** Извлечение данных */
    func fetchData() {
        do {
            try fetchedResultsController.performFetch()
        } catch {
            print("Showcase fetch error")
        }
    }

}

extension SuppliersDataContext: TableDataCollection {

    func registerCells(for tableView: UITableView) {
        SupplierTableViewCellModel.registerCell(for: tableView)
    }

    func numberOfSections(for tableView: UITableView) -> Int {
        return fetchedResultsController.sections?.count ?? 0
    }

    func numberOfRows(for tableView: UITableView, in section: Int) -> Int {
        return fetchedResultsController.sections?[section].numberOfObjects ?? 0
    }

    func dequeueReusableCell(for tableView: UITableView, in viewController: UIViewController?, at indexPath: IndexPath) -> UITableViewCell {
        let cell = SupplierTableViewCellModel.cell(for: tableView, at: indexPath)
        let supplier = fetchedResultsController.object(at: indexPath)
        SupplierTableViewCellModel.setup(cell, with: supplier)
        return cell
    }

    func titleForHeader(for tableView: UITableView, in section: Int) -> String? {
        return fetchedResultsController.sections?[section].name
    }

    /**
     Выбор пункта меню. Открытие контакта.
     */
    func didSelect(for tableView: UITableView, in viewController: UIViewController?, at indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let supplier = fetchedResultsController.object(at: indexPath)
        viewController?.navigationController?.pushViewController(UIStoryboard.Suppliers.details(supplier: supplier, dataController: dataController).viewController, animated: true)
    }

}

extension SuppliersDataContext {

    func updateSearchResults(with searchText: String, in viewController: UIViewController) {
        let request = fetchedResultsController.fetchRequest
        if searchText.isEmpty {
            request.predicate = nil
        } else {
            request.predicate = NSPredicate(format: "name contains[c] %@", searchText)
        }
        fetchData()

        if let resultsController = viewController as? UITableViewController {
            resultsController.tableView.reloadData()
        }
    }

}

extension SuppliersDataContext {

    /**
     Создание нового поставщика (производителя) товаров

     - parameter image: Изображение, используется в качестве основного
     - parameter preview: Изображение, используется в качестве превью
     - parameter presenter: Контроллер, в котором будет открыта форма редактирования
     */
    func createSupplier(image: UIImage? = nil, preview: UIImage? = nil, presenter: UIViewController?) {
        let dataContext = dataController.newBackgroundContext()
        // Создаем производителя
        let supplier = Supplier.create(in: dataContext)
        supplier.picture = preview
        if let identifier = showcase?.identifier {
            supplier.showcase = Showcase.showcase(identifier: identifier, in: dataContext)
        }

        let modifyViewController = UIStoryboard.Suppliers.modify(supplier: supplier, dataContext: dataContext, image: image, delegate: self).viewController
        presenter?.navigationController?.pushViewController(modifyViewController, animated: true)
    }

    /**
     Удаление товара
    */
    func removeSupplier(at indexPath: IndexPath) {
        let supplier = fetchedResultsController.object(at: indexPath)
        dataController.viewContext.delete(supplier)
        dataController.save()
        NotificationCenter.default.post(name: SuppliersDataContextDidChangeSuccessfuly, object: nil)
    }

    /**
     Редактирование  выставки

     - parameter presenter: Контроллер, в котором будет открыта форма редактирования
     */
    func modifyShowcase(presenter: UIViewController) {
        let dataContext = dataController.newBackgroundContext()
        // Достаем выставку
        if let identifier = showcase?.identifier {
            let showcase = Showcase.showcase(identifier: identifier, in: dataContext)
            let modifyViewController = UIStoryboard.Showcases.modify(showcase: showcase, dataContext: dataContext, delegate: (presenter as! ShowcaseModifyViewControllerDelegate)).viewController
            presenter.present(modifyViewController, animated: true)
        }
    }

}

extension SuppliersDataContext: SupplierModifyViewControllerDelegate {

    func supplierModifyViewController(_ supplierModifyViewController: SupplierModifyViewController, didChange supplier: Supplier?, in dataContext: NSManagedObjectContext) {
        dataController.save(changesIn: dataContext)
        NotificationCenter.default.post(name: SuppliersDataContextDidChangeSuccessfuly, object: nil)
        supplierModifyViewController.dismiss(animated: true)
    }

    func supplierModifyViewControllerWillCancel(_ supplierModifyViewController: SupplierModifyViewController) {
        supplierModifyViewController.dismiss(animated: true)
    }

}
