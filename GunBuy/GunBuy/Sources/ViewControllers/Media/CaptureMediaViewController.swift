
import UIKit
import CameraManager
import CoreLocation

protocol CaptureMediaViewControllerDelegate: class {

    func captureMediaViewController(_ captureMediaViewController: CaptureMediaViewController?, didFinishWithImage image: UIImage?, preview: UIImage?)

    func captureMediaViewController(_ captureMediaViewController: CaptureMediaViewController?, didFinishWithVideoAt url: URL?, preview: UIImage?)

    func captureMediaViewControllerWillCancel(_ captureMediaViewController: CaptureMediaViewController)

}

extension CaptureMediaViewControllerDelegate {

    func captureMediaViewController(_ captureMediaViewController: CaptureMediaViewController?, didFinishWithImage image: UIImage?, preview: UIImage?) { }

    func captureMediaViewController(_ captureMediaViewController: CaptureMediaViewController?, didFinishWithVideoAt url: URL?, preview: UIImage?) { }

}

class CaptureMediaViewController: UIViewController {

    // MARK:- Delegate

    weak var delegate: CaptureMediaViewControllerDelegate?

    // MARK:- Outlets

    @IBOutlet fileprivate weak var cameraView: UIView!

    @IBOutlet fileprivate weak var permissionsView: UIView! {
        didSet {
            permissionsView.isHidden = true
        }
    }

    @IBOutlet fileprivate weak var shootButton: UIButton! {
        didSet {
            shootButton.setBackgroundImage(UIImage(style: .shoot), for: .normal)
        }
    }

    // MARK:- Values

    let cameraManager = CameraManager()

    override func viewDidLoad() {
        super.viewDidLoad()
        //navigationController?.setNavigationBarHidden(true, animated: false)

        cameraManager.shouldEnableExposure = true

        cameraManager.shouldFlipFrontCameraImage = false
        cameraManager.showAccessPermissionPopupAutomatically = false

        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .authorizedAlways, .authorizedWhenInUse:
                self.cameraManager.shouldUseLocationServices = true
            default:
                self.cameraManager.shouldUseLocationServices = false
            }
        }

        switch cameraManager.currentCameraStatus() {
        case .ready:
            addCameraToView()
        default:
            permissionsView.isHidden = false
        }

        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(doCancel))
    }

    fileprivate func addCameraToView() {
        //cameraManager.addPreviewLayerToView(cameraView, newCameraOutputMode: CameraOutputMode.videoWithMic)
        cameraManager.addPreviewLayerToView(cameraView)
        cameraManager.showErrorBlock = { [weak self] (erTitle: String, erMessage: String) -> Void in
            let alertController = UIAlertController(title: erTitle, message: erMessage, preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "OK", style: .default))
            self?.present(alertController, animated: true, completion: nil)
        }
    }

    // MARK:- Actions

    @IBAction func doAskPermission(_ sender: UIButton) {
        self.cameraManager.askUserForCameraPermission({ [weak self] permissionGranted in
            if permissionGranted {
                self?.permissionsView.isHidden = true
                self?.addCameraToView()
            } else {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(URL(string:UIApplication.openSettingsURLString)!)
                } else {
                    // Fallback on earlier versions
                }
            }
        })
    }

    @IBAction func doCancel(_ sender: UIBarButtonItem) {
        delegate?.captureMediaViewControllerWillCancel(self)
    }

    @IBAction func takePhoto(_ sender: UIButton) {

        guard UIImagePickerController.isSourceTypeAvailable(.camera) else {
            // Очевидно эмулятор
            delegate?.captureMediaViewController(self, didFinishWithImage: nil, preview: nil)
            return
        }

        switch cameraManager.cameraOutputMode {
        case .stillImage:
            cameraManager.capturePictureWithCompletion { [weak self] result in
                switch result {
                case .failure:
                    self?.cameraManager.showErrorBlock("Error occurred", "Cannot save picture.")
                case .success(let content):
                    if let uiImage = content.asImage {
                        let uiPreview = uiImage.resize(to: CGSize(width: 512, height: 512))
                        self?.delegate?.captureMediaViewController(self, didFinishWithImage: uiImage, preview: uiPreview)
                    }
                }
            }
        case .videoWithMic, .videoOnly:
            shootButton.isSelected = !shootButton.isSelected
            shootButton.setTitle("", for: UIControl.State.selected)
            //shootButton.backgroundColor = shootButton.isSelected ? redColor : lightBlue
            if sender.isSelected {
                cameraManager.startRecordingVideo()
            } else {
                cameraManager.stopVideoRecording({ [weak self] (videoURL, error) -> Void in
                    if error == nil {
                        self?.delegate?.captureMediaViewController(self, didFinishWithVideoAt: videoURL, preview: nil)
                    } else {
                        self?.cameraManager.showErrorBlock("Error occurred", "Cannot save video.")
                    }
                })
            }
        }

    }

}
