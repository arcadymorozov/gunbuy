/**

 Снимок / Видео

 */

import UIKit
import AVFoundation
import VideoToolbox
import CameraManager

enum MediaPickerStyle {
    case photo
    case video
}

typealias MediaPickerHandler = (AnyObject?, UIImage?) -> Void

class MediaPickerViewController: UIViewController {

    static func instantiate(with style: MediaPickerStyle, completion: MediaPickerHandler?) -> UIViewController {
        let mediaPickerViewController1 = UIStoryboard(name: "MediaPicker", bundle: nil).instantiateInitialViewController()
        dump(mediaPickerViewController1)
        guard let mediaPickerViewController = UIStoryboard(name: "MediaPicker", bundle: nil).instantiateInitialViewController() as? MediaPickerViewController else {
            fatalError("MediaPickerViewController not found")
        }
        mediaPickerViewController.completion = completion
        return mediaPickerViewController
    }

    let cameraManager = CameraManager()

    var completion: MediaPickerHandler?

    @IBOutlet fileprivate weak var cameraView: UIView!

    @IBOutlet fileprivate weak var shootButton: UIButton! {
        didSet {
            shootButton.setBackgroundImage(UIImage(style: .shoot), for: .normal)
        }
    }

    var captureSession: AVCaptureSession!
    var cameraOutput: AVCapturePhotoOutput!
    var previewLayer: AVCaptureVideoPreviewLayer!

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(doCancel))
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        startCamera()
    }

    func startCamera() {
        captureSession = AVCaptureSession()
        captureSession.sessionPreset = AVCaptureSession.Preset.photo
        cameraOutput = AVCapturePhotoOutput()

        if let device = AVCaptureDevice.default(for: .video),
            let input = try? AVCaptureDeviceInput(device: device) {
            if (captureSession.canAddInput(input)) {
                captureSession.addInput(input)
                if (captureSession.canAddOutput(cameraOutput)) {
                    captureSession.addOutput(cameraOutput)
                    previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
                    previewLayer.frame = cameraView.frame
                    cameraView.layer.addSublayer(previewLayer)
                    captureSession.startRunning()
                }
            } else {
                print("issue here : captureSesssion.canAddInput")
            }
        } else {
            print("some problem here")
        }
    }

    @IBAction func takePhoto(_ sender: UIButton) {
        shootButton.isEnabled = false

        if TARGET_OS_SIMULATOR != 0 {
            completion?(nil, nil)
            return
        }

        let settings = AVCapturePhotoSettings()
        let previewPixelType = settings.availablePreviewPhotoPixelFormatTypes.first!

        settings.previewPhotoFormat = [
            kCVPixelBufferPixelFormatTypeKey: previewPixelType,
            kCVPixelBufferWidthKey: 512,
            kCVPixelBufferHeightKey: 512
            ] as [String: Any]

        /*
         settings.embeddedThumbnailPhotoFormat = [
         AVVideoCodecKey: AVVideoCodecType.jpeg,
         AVVideoWidthKey: 1024,
         AVVideoHeightKey: 1024,
         ]
         */

        cameraOutput.capturePhoto(with: settings, delegate: self)
    }

    @IBAction func doCancel(_ sender: UIBarButtonItem) {
        dismiss(animated: true)
    }

}

extension MediaPickerViewController: AVCapturePhotoCaptureDelegate {

    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {

        if let error = error {
            print("error occured : \(error.localizedDescription)")
        }

        guard let previewPixelBuffer = photo.previewPixelBuffer else {
            completion?(nil, nil)
            return
        }

        var uiPreview: UIImage?
        var uiImage: UIImage?

        var cgPreview: CGImage?
        VTCreateCGImageFromCVPixelBuffer(previewPixelBuffer, options: nil, imageOut: &cgPreview)
        if let cgPreview = cgPreview {
            uiPreview = UIImage(cgImage: cgPreview, scale: 1.0, orientation: UIImage.Orientation.right)
        }

        if let dataImage = photo.fileDataRepresentation() {
            let dataProvider = CGDataProvider(data: dataImage as CFData)
            let cgImage = CGImage(jpegDataProviderSource: dataProvider!, decode: nil, shouldInterpolate: true, intent: .defaultIntent)
            if let cgImage = cgImage {
                uiImage = UIImage(cgImage: cgImage, scale: 1.0, orientation: UIImage.Orientation.right)
            }
        }

        completion?(uiImage, uiPreview)
    }

}
