/**
 
    Авторизация
 
 */

import UIKit
import NetworkStorage

class AuthorizationViewController: UIViewController {

    @IBOutlet fileprivate weak var loginTextField: UITextField!

    @IBOutlet fileprivate weak var passwordTextField: UITextField!

    @IBOutlet fileprivate weak var authButton: UIButton! {
        didSet {
            authButton.setTitle("Login".localized, for: .normal)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Login".localized
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel".localized, style: .plain, target: self, action: #selector(doCancel))
    }
    
    @IBAction func doAuthorize(_ sender: UIButton) {
        if let login = loginTextField.text, let password = passwordTextField.text {
            AuthorizationController.authorize(with: login, password: password) { [weak self] (success, message) in
                // Авторизация завершена со статусом success
                if success {
                    
                } else if let message = message {
                    let alertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
                    alertController.addAction(UIAlertAction(title: "Ok".localized, style: .default))
                    self?.present(alertController, animated: true)
                }
            }
        }
    }

    @IBAction func doCancel(_ sender: UIBarButtonItem) {
        dismiss(animated: true)
    }

}
