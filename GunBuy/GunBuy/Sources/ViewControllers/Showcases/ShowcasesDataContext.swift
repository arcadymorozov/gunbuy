/**

 Выставки, предприятия, поставщики и т.п.
 Контекст данных

 */

import UIKit
import CoreData

class ShowcasesDataContext {

    /** Контроллер для извлечения данных */
    var fetchedResultsController: NSFetchedResultsController<Showcase>!

    /** Контроллер доступа к базе */
    var dataController: CoreDataController!

    var title: String {
        return "Showcases".localized
    }

    var isEmpty: Bool {
        return fetchedResultsController.sections!.isEmpty
    }

    init(dataController: CoreDataController) {
        self.dataController = dataController
        self.fetchedResultsController = Showcase.fetchedResultsController(with: dataController.viewContext)
    }

    /** Извлечение данных */
    func fetchData() {
        do {
            try fetchedResultsController.performFetch()
        } catch {
            print("Showcase fetch error")
        }
    }

}

extension ShowcasesDataContext: TableDataCollection {

    func registerCells(for tableView: UITableView) {
        ShowcaseTableViewCellModel.registerCell(for: tableView)
        HeaderTableViewCellModel.registerCell(for: tableView)
    }

    func numberOfSections(for tableView: UITableView) -> Int {
        return fetchedResultsController.sections?.count ?? 0
    }

    func numberOfRows(for tableView: UITableView, in section: Int) -> Int {
        return fetchedResultsController.sections?[section].numberOfObjects ?? 0
    }

    func dequeueReusableCell(for tableView: UITableView, in viewController: UIViewController?, at indexPath: IndexPath) -> UITableViewCell {
        let cell = ShowcaseTableViewCellModel.cell(for: tableView, at: indexPath)
        let showcase = fetchedResultsController.object(at: indexPath)
        ShowcaseTableViewCellModel.setup(cell, with: showcase)
        return cell
    }

    func titleForHeader(for tableView: UITableView, in section: Int) -> String? {
        if let sectionName = fetchedResultsController.sections?[section].name, let position = Int(sectionName) {
            return ShowcaseStyle(rawValue: position)?.name
        }
        return nil
    }

    func viewForHeader(for tableView: UITableView, in section: Int) -> UIView? {
        let cell = HeaderTableViewCellModel.cell(for: tableView, at: IndexPath(row: 0, section: section))
        HeaderTableViewCellModel.setup(cell, with: titleForHeader(for: tableView, in: section))
        return cell.contentView
    }

    /**
     Выбор пункта меню.
     */
    func didSelect(for tableView: UITableView, in viewController: UIViewController?, at indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let showcase = fetchedResultsController.object(at: indexPath)
        let suppliersViewController = UIStoryboard.Suppliers.main(showcase: showcase, dataController: dataController).viewController
        viewController?.navigationController?.pushViewController(suppliersViewController, animated: true)
    }

}

extension ShowcasesDataContext {

    func updateSearchResults(with searchText: String, in viewController: UIViewController) {
        let request = fetchedResultsController.fetchRequest
        if searchText.isEmpty {
            request.predicate = nil
        } else {
            request.predicate = NSPredicate(format: "name contains[c] %@", searchText)
        }
        fetchData()

        if let resultsController = viewController as? UITableViewController {
            resultsController.tableView.reloadData()
        }
    }

}

extension ShowcasesDataContext {

    /**
     Создание новой выставки

     - parameter image: Изображение, используется в качестве основного
     - parameter preview: Изображение, используется в качестве превью
     - parameter presenter: Контроллер, в котором будет открыта форма редактирования
     */
    func createShowcase(with showcaseStyle: ShowcaseStyle, presenter: UIViewController) {
        let dataContext = dataController.newBackgroundContext()
        // Создаем новую выставку
        let showcase = Showcase.create(in: dataContext)
        showcase.style = Int16(showcaseStyle.rawValue)

        let modifyViewController = UIStoryboard.Showcases.modify(showcase: showcase, dataContext: dataContext, delegate: (presenter as! ShowcaseModifyViewControllerDelegate)).viewController
        presenter.present(modifyViewController, animated: true)
    }

}
