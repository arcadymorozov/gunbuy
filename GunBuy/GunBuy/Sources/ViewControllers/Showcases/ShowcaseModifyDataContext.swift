/**

 Редактирование выставки

 */

import UIKit
import CoreData

class ShowcaseModifyDataContext {

    var showcase: Showcase?

    let dataContext: NSManagedObjectContext!

    enum Row {
        case name
        case place
        case startDate
        case endDate

        var title: String {
            switch self {
            case .name: return "Name".localized
            case .place: return "City".localized
            case .startDate: return "Start Date".localized
            case .endDate: return "End Date".localized
            }
        }

        func value(for showcase: Showcase?) -> Any? {
            switch self {
            case .name: return showcase?.name
            case .place: return showcase?.details
            case .startDate: return "Start Date".localized
            case .endDate: return "End Date".localized
            }
        }
    }

    var activeResponder: UIResponder?

    var rows: [Row] = [.name, .place]

    var title: String {
        return showcase?.showcaseStyle?.title ?? "Showcase".localized
    }

    /** Создать контекст для работы с витриной */
    init(with showcase: Showcase?, in dataContext: NSManagedObjectContext) {
        self.showcase = showcase
        self.dataContext = dataContext
    }

    /** Убираем клавиатуру */
    func resignResponder() {
        activeResponder?.resignFirstResponder()
        activeResponder = nil
    }

    /** Валидация данных */
    func validate(_ completion: (String) -> Void) -> Bool {
        guard let name = showcase?.name, !name.isEmpty else {
            completion("Enter Name".localized)
            return false
        }
        return true
    }

}

extension ShowcaseModifyDataContext: TableDataCollection {

    func registerCells(for tableView: UITableView) {
        TextFieldTableViewCellModel.registerCell(for: tableView)
    }

    func numberOfSections(for tableView: UITableView) -> Int {
        return 1
    }

    func numberOfRows(for tableView: UITableView, in section: Int) -> Int {
        return rows.count
    }

    func dequeueReusableCell(for tableView: UITableView, in viewController: UIViewController?, at indexPath: IndexPath) -> UITableViewCell {
        let cell = TextFieldTableViewCellModel.cell(for: tableView, at: indexPath)
        TextFieldTableViewCellModel.setup(cell, title: rows[indexPath.row].title, value: rows[indexPath.row].value(for: showcase) as? String, style: .text, delegate: self, at: indexPath)
        return cell
    }

}

extension ShowcaseModifyDataContext: TextFieldTableViewCellDelegate {

    func textFieldTableViewCell(_ cell: TextFieldTableViewCell, didBeginEditing textField: UITextField, at indexPath: IndexPath?) {
        activeResponder = textField
    }

    func textFieldTableViewCell(_ cell: TextFieldTableViewCell, didChange value: String?, at indexPath: IndexPath?) {
        activeResponder = nil
        if let indexPath = indexPath {
            switch rows[indexPath.row] {
            case .name: showcase?.name = value
            case .place: showcase?.details = value
            default: break
            }
        }
    }

}
