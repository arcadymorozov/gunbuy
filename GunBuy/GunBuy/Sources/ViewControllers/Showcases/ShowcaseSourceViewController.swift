/**

 Выбор источника (выставки)

 */

import UIKit

/**
 Оповещение об изменениях..
 */
protocol ShowcaseSourceViewControllerDelegate: class {

    func showcaseSourceViewController(_ showcaseSourceViewController: ShowcaseSourceViewController, didSelect showcaseStyle: ShowcaseStyle)

}

class ShowcaseSourceViewController: UIViewController {

    weak var delegate: ShowcaseSourceViewControllerDelegate?

    @IBOutlet fileprivate var titleLabel: UILabel! {
        didSet {
            titleLabel.text = "Where are you going to work?".localized
        }
    }

    @IBOutlet fileprivate var subtitleLabel: UILabel! {
        didSet {
            subtitleLabel.text = "Or work for".localized
        }
    }

    @IBOutlet fileprivate weak var exhibitionButton: UIButton! {
        didSet {
            exhibitionButton.tag = ShowcaseStyle.exhibition.tag
            exhibitionButton.setBackgroundImage(UIImage(style: .exhibition, frame: CGRect(x: 0, y: 0, width: 160, height: 160)), for: .normal)
        }
    }

    @IBOutlet fileprivate weak var exhibitionLabel: UILabel! {
        didSet {
            exhibitionLabel.text = "Exhibitions".localized
        }
    }

    @IBOutlet fileprivate weak var marketButton: UIButton! {
        didSet {
            marketButton.tag = ShowcaseStyle.market.tag
            marketButton.setBackgroundImage(UIImage(style: .market, frame: CGRect(x: 0, y: 0, width: 160, height: 160)), for: .normal)
        }
    }

    @IBOutlet fileprivate weak var marketLabel: UILabel! {
        didSet {
            marketLabel.text = "Markets".localized
        }
    }

    @IBOutlet fileprivate weak var factoryButton: UIButton! {
        didSet {
            factoryButton.tag = ShowcaseStyle.factory.tag
            factoryButton.setBackgroundImage(UIImage(style: .factory, frame: CGRect(x: 0, y: 0, width: 160, height: 160)), for: .normal)
        }
    }

    @IBOutlet fileprivate weak var factoryLabel: UILabel! {
        didSet {
            factoryLabel.text = "Factorys".localized
        }
    }

    @IBOutlet fileprivate weak var wholesaleButton: UIButton! {
        didSet {
            wholesaleButton.tag = ShowcaseStyle.wholesale.tag
            wholesaleButton.setBackgroundImage(UIImage(style: .wholesale, frame: CGRect(x: 0, y: 0, width: 160, height: 160)), for: .normal)
        }
    }

    @IBOutlet fileprivate weak var wholesaleLabel: UILabel! {
        didSet {
            wholesaleLabel.text = "Wholesalers".localized
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.isTranslucent = true
        title = "New Showcase".localized
    }

    @IBAction func doSelectSource(_ sender: UIButton) {
        delegate?.showcaseSourceViewController(self, didSelect: ShowcaseStyle(rawValue: sender.tag) ?? .unspecified)
    }

}
