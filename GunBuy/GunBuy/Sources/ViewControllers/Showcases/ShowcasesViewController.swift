/**

 Выставки, предприятия, поставщики и т.п.

 */

import UIKit
import CoreData

class ShowcasesViewController: UIViewController {

    @IBOutlet fileprivate var tableView: UITableView! {
        didSet {
            tableView.rowHeight = UITableView.automaticDimension
            tableView.estimatedRowHeight = 70.0
            tableView.tableFooterView = UIView()
            tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 100, right: 0)
            // Register NIB
            context.registerCells(for: tableView)
        }
    }

    @IBOutlet fileprivate var titleLabel: UILabel! {
        didSet {
            titleLabel.text = "No Showcases".localized
        }
    }

    @IBOutlet fileprivate var subtitleLabel: UILabel! {
        didSet {
            subtitleLabel.text = "All Producers Exhibition...".localized
        }
    }

    @IBOutlet fileprivate weak var sorrowImageView: UIImageView! {
        didSet {
            sorrowImageView.image = UIImage(style: .sorrow, frame: CGRect(x: 0, y: 0, width: 160, height: 160))
        }
    }

    @IBOutlet fileprivate var addButton: UIButton! {
        didSet {
            addButton.setBackgroundImage(UIImage(style: .roundedAdd, frame: CGRect(x: 0, y: 0, width: 480, height: 480)), for: .normal)
        }
    }

    @IBOutlet fileprivate var addLabel: UILabel! {
        didSet {
            addLabel.text = "New Showcase".localized
        }
    }

    /** Контекст данных */
    var context: ShowcasesDataContext!

    private struct SearchControllerRestorableState {
        var wasActive = false
        var wasFirstResponder = false
    }

    /// Search controller to help us with filtering.
    private var searchController: UISearchController!

    /// Restoration state for UISearchController
    private var restoredState = SearchControllerRestorableState()

    override func viewDidLoad() {
        super.viewDidLoad()
        title = context.title
        fetchData()

        //MARK: - Search Controller
        let searchResultsController = SearchResultsController(context: context, presenter: self)
        searchController = UISearchController(searchResultsController: searchResultsController)
        searchController.searchResultsUpdater = self
        searchController.searchBar.autocapitalizationType = .none
        if #available(iOS 11.0, *) {
            navigationItem.searchController = searchController
            navigationItem.hidesSearchBarWhenScrolling = true
        } else {
            tableView.tableHeaderView = searchController.searchBar
        }
        //searchController.delegate = self
        searchController.dimsBackgroundDuringPresentation = false // The default is true.
        searchController.searchBar.delegate = self // Monitor when the search button is tapped.
        definesPresentationContext = true
        
        //MARK: - Authorization
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Login".localized, style: .plain, target: self, action: #selector(doLogin))
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchData()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // Restore the searchController's active state.
        if restoredState.wasActive {
            searchController.isActive = restoredState.wasActive
            restoredState.wasActive = false

            if restoredState.wasFirstResponder {
                searchController.searchBar.becomeFirstResponder()
                restoredState.wasFirstResponder = false
            }
        }
    }

    /** Извлечение данных в контексте */
    func fetchData() {
        context.fetchData()
        reloadData()
    }

    /** Обновление данных */
    func reloadData() {
        tableView.reloadData()
        tableView.isHidden = context.isEmpty
    }

    @IBAction func doAdd(_ sender: UIBarButtonItem) {
        let sourceViewController = UIStoryboard.Showcases.sources(delegate: self).viewController
        navigationController?.pushViewController(sourceViewController, animated: true)
    }

    @IBAction func doLogin(_ sender: UIBarButtonItem) {
        let authorizationViewController = UIStoryboard.Authorization.main.viewController
        present(authorizationViewController, animated: true)
    }

}

extension ShowcasesViewController: UITableViewDelegate, UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return context.numberOfSections(for: tableView)
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return context.numberOfRows(for: tableView, in: section)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return context.dequeueReusableCell(for: tableView, in: self, at: indexPath)
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        context.didSelect(for: tableView, in: self, at: indexPath)
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return context.titleForHeader(for: tableView, in: section)
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return context.viewForHeader(for: tableView, in: section)
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60.0
    }

}

// MARK: - UISearchBarDelegate

extension ShowcasesViewController: UISearchBarDelegate {

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }

}

extension ShowcasesViewController: UISearchResultsUpdating {

    func updateSearchResults(for searchController: UISearchController) {
        context.updateSearchResults(with: searchController.searchBar.text!, in: searchController.searchResultsController!)
    }

}

// Выбор типа Showcase

extension ShowcasesViewController: ShowcaseSourceViewControllerDelegate {

    func showcaseSourceViewController(_ showcaseSourceViewController: ShowcaseSourceViewController, didSelect showcaseStyle: ShowcaseStyle) {
        context.createShowcase(with: showcaseStyle, presenter: self)
    }

}

//MARK: - Создание Showcase -

extension ShowcasesViewController: ShowcaseModifyViewControllerDelegate {


    /** Витрина успешно создана */
    func showcaseModifyViewController(_ showcaseModifyViewController: ShowcaseModifyViewController, didChange showcase: Showcase?, in dataContext: NSManagedObjectContext) {
        context.dataController.save(changesIn: dataContext)
        navigationController?.popToRootViewController(animated: false)
        dismiss(animated: true)
    }

    func showcaseModifyViewControllerDidCancel(_ showcaseModifyViewController: ShowcaseModifyViewController) {
        dismiss(animated: true)
    }

}
