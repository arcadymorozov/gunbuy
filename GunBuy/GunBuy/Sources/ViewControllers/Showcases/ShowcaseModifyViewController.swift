/**

 Выставка
 Создание / Редактирование

 */

import UIKit
import CoreData

/**
 Оповещение об изменениях..
 */
protocol ShowcaseModifyViewControllerDelegate: class {

    func showcaseModifyViewController(_ showcaseModifyViewController: ShowcaseModifyViewController, didChange showcase: Showcase?, in dataContext: NSManagedObjectContext)

    func showcaseModifyViewControllerDidCancel(_ showcaseModifyViewController: ShowcaseModifyViewController)

}

class ShowcaseModifyViewController: UIViewController {

    @IBOutlet fileprivate var tableView: UITableView! {
        didSet {
            tableView.rowHeight = UITableView.automaticDimension
            tableView.estimatedRowHeight = 70.0
            tableView.tableFooterView = UIView()
            tableView.contentInset = UIEdgeInsets(top: 12, left: 0, bottom: 12, right: 0)
        }
    }

    var context: ShowcaseModifyDataContext!

    weak var delegate: ShowcaseModifyViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        title = context.title

        context.registerCells(for: tableView)

        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(doCancel))
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doDone))
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let cell = tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? TextFieldTableViewCell {
            cell.becomeResponder()
        }
    }

    /**
     Отмена создания мероприятия
     */
    @IBAction func doCancel(_ sender: UIBarButtonItem) {
        context.resignResponder()
        delegate?.showcaseModifyViewControllerDidCancel(self)
    }

    /**
     Создания мероприятия
     */
    @IBAction func doDone(_ sender: UIBarButtonItem) {
        context.resignResponder()
        if context.validate({ [weak self] (message) in
            let alertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Ok".localized, style: .default))
            self?.present(alertController, animated: true)
        }) {
            delegate?.showcaseModifyViewController(self, didChange: context.showcase, in: context.dataContext)
        }
    }

}

extension ShowcaseModifyViewController: UITableViewDelegate, UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return context.numberOfSections(for: tableView)
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return context.numberOfRows(for: tableView, in: section)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return context.dequeueReusableCell(for: tableView, in: self, at: indexPath)
    }

}

