/**

 Контроллер отображения поисковых результатов
 Базовый класс. Возможно самостоятельное использование.

 */

import UIKit

class SearchResultsController: UITableViewController {

    var context: TableDataCollection!

    var presenter: UIViewController!

    /**
     Инициализатор.

     - parameter context: Контекст данных
     - parameter presenter: Родительский контроллер для пользовательского взаимодействия
     */
    init(context: TableDataCollection, presenter: UIViewController) {
        self.context = context
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 70.0
        tableView.tableFooterView = UIView()
        // Register NIB
        context.registerCells(for: tableView)
    }

}

extension SearchResultsController {

    override func numberOfSections(in tableView: UITableView) -> Int {
        return context.numberOfSections(for: tableView)
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return context.numberOfRows(for: tableView, in: section)
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return context.dequeueReusableCell(for: tableView, in: presenter, at: indexPath)
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        context.didSelect(for: tableView, in: presenter, at: indexPath)
    }

}
