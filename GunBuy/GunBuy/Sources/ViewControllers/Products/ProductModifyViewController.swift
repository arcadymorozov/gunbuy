/**

 Товар. Создание / Редактирование

 */

import UIKit
import CoreData

/**
 Оповещение об изменениях..
 */
protocol ProductModifyViewControllerDelegate {

    func productModifyViewController(_ productModifyViewController: ProductModifyViewController, didChange product: Product?, in dataContext: NSManagedObjectContext)

    func productModifyViewControllerWillCancel(_ productModifyViewController: ProductModifyViewController)

}

class ProductModifyViewController: UITableViewController {
/*
    @IBOutlet fileprivate var tableView: UITableView! {
        didSet {
            tableView.rowHeight = UITableView.automaticDimension
            tableView.estimatedRowHeight = 70.0
            tableView.tableFooterView?.isHidden = true
            tableView.contentInset = UIEdgeInsets(top: 12, left: 0, bottom: 12, right: 0)
        }
    }
*/
    @IBOutlet fileprivate var bottomLayoutConstraint: NSLayoutConstraint!

    var context: ProductModifyDataContext!

    var delegate: ProductModifyViewControllerDelegate?

    var isModal: Bool {
        return context.isModal
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        title = context.title
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 70.0
        tableView.tableFooterView?.isHidden = true
        tableView.contentInset = UIEdgeInsets(top: 12, left: 0, bottom: 12, right: 0)

        // В модальном режиме кнопка закрытия окна
        //if context.isModal {
        //    navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(doCancel))
        //}
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doDone))

        toolbarItems = [
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
            UIBarButtonItem(title: "Text".localized, style: .plain, target: self, action: #selector(doAddText)),
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
            UIBarButtonItem(title: "Image".localized, style: .plain, target: self, action: #selector(doAddImage)),
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
            UIBarButtonItem(title: "Photo".localized, style: .plain, target: self, action: #selector(doAddPhoto)),
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
            UIBarButtonItem(title: "Video".localized, style: .plain, target: self, action: #selector(doAddVideo)),
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
            UIBarButtonItem(title: "Property".localized, style: .plain, target: self, action: #selector(doAddProperty)),
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        ]

        // Register NIB
        context.registerCells(for: tableView)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setToolbarHidden(false, animated: true)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)

        navigationController?.setToolbarHidden(true, animated: true)
    }

    @IBAction func doCancel(_ sender: UIBarButtonItem) {
        context.resignResponder()
        delegate?.productModifyViewControllerWillCancel(self)
    }

    @IBAction func doDone(_ sender: UIBarButtonItem) {
        context.resignResponder()
        if context.validate({ [weak self] (message) in
            let alertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Ok".localized, style: .default))
            self?.present(alertController, animated: true)
        }) {
            delegate?.productModifyViewController(self, didChange: context.product, in: context.dataContext)
        }
    }

    /**
        Добавляем текстовый блок
     */
    @IBAction func doAddText(_ sender: UIBarButtonItem) {
        _ = context.insertMention(.text)
        tableView.reloadData()
        // Теоретически это последняя ячейка
        let rowsCount = context.numberOfRows(for: tableView, in: 0) - 1
        let indexPath = IndexPath(row: rowsCount, section: 0)
        //tableView.scrollToRow(at: indexPath, at: .top, animated: true)
        if let cell = tableView.cellForRow(at: indexPath) as? TextMentionTableViewCell {
            cell.becomeResponder()
        }
    }

    @IBAction func doAddImage(_ sender: UIBarButtonItem) {
        let sketchViewController = UIStoryboard.Sketch.main(image: nil, delegate: self).viewController
        present(sketchViewController, animated: true)
    }

    @IBAction func doAddPhoto(_ sender: UIBarButtonItem) {
        let mediaPickerViewController = MediaPickerViewController.instantiate(with: .photo) { [weak self] (image, preview) in
            if let preview = preview {
                let mention = self?.context.insertMention(.image)
                mention?.data = preview
                if let tableView = self?.tableView, let context = self?.context {
                    tableView.reloadData()
                    let rowsCount = context.numberOfRows(for: tableView, in: 0) - 1
                    let indexPath = IndexPath(row: rowsCount, section: 0)
                    tableView.scrollToRow(at: indexPath, at: .top, animated: true)
                }
            }
            self?.dismiss(animated: true)
        }
        present(mediaPickerViewController, animated: true)
    }

    @IBAction func doAddVideo(_ sender: UIBarButtonItem) {

    }

    @IBAction func doAddProperty(_ sender: UIBarButtonItem) {
        let propertiesViewController = UIStoryboard.Products.properties(delegate: self).viewController
        navigationController?.pushViewController(propertiesViewController, animated: true)
    }

}

extension ProductModifyViewController {

    override func numberOfSections(in tableView: UITableView) -> Int {
        return context.numberOfSections(for: tableView)
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return context.numberOfRows(for: tableView, in: section)
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return context.dequeueReusableCell(for: tableView, in: self, at: indexPath)
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        context.didSelect(for: tableView, in: self, at: indexPath)
    }

    // Edit Rows

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return context.canEditRow(for: tableView, at: indexPath)
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            // handle delete (by removing the data from your array and updating the tableview)
            let alertController = UIAlertController(title: nil, message: "Remove Mention?".localized, preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Yes".localized, style: .default, handler: { [weak self] (_) in
                self?.tableView.beginUpdates()
                self?.context.removeMention(at: indexPath)
                self?.tableView.deleteRows(at: [indexPath], with: .automatic)
                self?.tableView.endUpdates()
            }))
            alertController.addAction(UIAlertAction(title: "No".localized, style: .cancel))
            present(alertController, animated: true)
        }
    }

}

extension ProductModifyViewController: SketchViewControllerDelegate {

    func sketchViewController(_ sketchViewController: SketchViewController, didChange image: UIImage?) {
        dismiss(animated: true)
        let mention = context.insertMention(.image)
        mention.data = image
        tableView.reloadData()
        let rowsCount = tableView(self.tableView, numberOfRowsInSection: 0)
        let indexPath = IndexPath(row: rowsCount - 1, section: 0)
        tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
    }

    func sketchViewControllerWillCancel(_ sketchViewController: SketchViewController) {
        dismiss(animated: true)
    }

}

extension ProductModifyViewController: PropertiesViewControllerDelegate {

    func propertiesViewController(_ propertiesViewController: PropertiesViewController, willSelect property: Property?) {
        navigationController?.popViewController(animated: true)
        if let property = property {
            let mention = context.insertMention(.property)
            mention.data = ["title": property.rawValue] as AnyObject
            tableView.reloadData()
            // Теоретически это последняя ячейка
            let rowsCount = context.numberOfRows(for: tableView, in: 0) - 1
            let indexPath = IndexPath(row: rowsCount, section: 0)
            //tableView.scrollToRow(at: indexPath, at: .top, animated: true)
            if let cell = tableView.cellForRow(at: indexPath) as? TextMentionTableViewCell {
                cell.becomeResponder()
            }
        }
    }

}
