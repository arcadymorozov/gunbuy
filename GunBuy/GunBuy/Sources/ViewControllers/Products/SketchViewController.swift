/**

 Товар

 */

import UIKit

/**
 Оповещение об изменениях..
 */
protocol SketchViewControllerDelegate: class {

    func sketchViewController(_ sketchViewController: SketchViewController, didChange image: UIImage?)

    func sketchViewControllerWillCancel(_ sketchViewController: SketchViewController)

}

class SketchViewController: UIViewController {

    // Основная фотография
    @IBOutlet fileprivate var photoImageView: UIImageView!

    // здесь отрисовываем нарисованное с canvas
    @IBOutlet fileprivate var sketchImageView: UIImageView!

    // здесь рисуем жестом
    @IBOutlet fileprivate var canvasImageView: UIImageView!

    @IBOutlet fileprivate var nameLabel: UILabel!

    weak var delegate: SketchViewControllerDelegate?

    var context: SketchDataContext!

    var lastPoint = CGPoint.zero

    var red: CGFloat = 255.0
    var green: CGFloat = 0.0
    var blue: CGFloat = 0.0

    var brushWidth: CGFloat = 4.0
    var brushOpacity: CGFloat = 1.0

    var swiped = false

    override func viewDidLoad() {
        super.viewDidLoad()
        title = context.title

        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(doCancel))
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doDone))

        reloadData()
    }

    func reloadData() {
        photoImageView.image = context.image
    }

    @IBAction func doCancel(_ sender: UIBarButtonItem) {
        delegate?.sketchViewControllerWillCancel(self)
    }

    @IBAction func doDone(_ sender: UIBarButtonItem) {
        // переносим изображение со sketch на photo
        UIGraphicsBeginImageContext(photoImageView.frame.size)
        photoImageView.image?.draw(in: CGRect(x: 0, y: 0, width: photoImageView.frame.size.width, height: photoImageView.frame.size.height), blendMode: .normal, alpha: 1.0)
        sketchImageView.image?.draw(in: CGRect(x: 0, y: 0, width: photoImageView.frame.size.width, height: photoImageView.frame.size.height), blendMode: .normal, alpha: brushOpacity)
        photoImageView.image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        delegate?.sketchViewController(self, didChange: photoImageView.image)
    }

}

extension SketchViewController {

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        swiped = false
        if let touch = touches.first {
            lastPoint = touch.location(in: sketchImageView)
        }
    }

    func drawLine(fromPoint: CGPoint, toPoint: CGPoint) {
        UIGraphicsBeginImageContext(sketchImageView.frame.size)
        let context = UIGraphicsGetCurrentContext()
        canvasImageView.image?.draw(in: CGRect(x: 0, y: 0, width: sketchImageView.frame.size.width, height: sketchImageView.frame.size.height))

        context?.move(to: fromPoint)
        context?.addLine(to: toPoint)

        context?.setLineCap(.round)
        context?.setLineWidth(brushWidth)
        context?.setStrokeColor(red: red, green: green, blue: blue, alpha: 1.0)
        context?.setBlendMode(.normal)

        context?.strokePath()

        canvasImageView.image = UIGraphicsGetImageFromCurrentImageContext()
        canvasImageView.alpha = brushOpacity
        UIGraphicsEndImageContext()
    }

    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        swiped = true
        if let touch = touches.first {
            let currentPoint = touch.location(in: sketchImageView)
            drawLine(fromPoint: lastPoint, toPoint: currentPoint)

            lastPoint = currentPoint
        }
    }

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if !swiped {
            drawLine(fromPoint: lastPoint, toPoint: lastPoint)
        }

        // переносим изображение с canvas на sketch
        UIGraphicsBeginImageContext(sketchImageView.frame.size)
        sketchImageView.image?.draw(in: CGRect(x: 0, y: 0, width: sketchImageView.frame.size.width, height: sketchImageView.frame.size.height), blendMode: .normal, alpha: 1.0)
        canvasImageView.image?.draw(in: CGRect(x: 0, y: 0, width: sketchImageView.frame.size.width, height: sketchImageView.frame.size.height), blendMode: .normal, alpha: brushOpacity)
        sketchImageView.image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        canvasImageView.image = nil
    }

}
