/**

 Товар. Создание / Редактирование

 */

import UIKit
import Storage
import CoreData

class ProductModifyDataContext {

    /** Большое изображение */
    var image: UIImage?

    var sketchCoordinator: SketchCoordinator?

    /** Товар */
    var product: Product?

    let dataContext: NSManagedObjectContext!

    var title: String {
        return product?.name ?? product?.supplier?.name ?? "Product".localized
    }

    let isModal: Bool!

    enum Row {
        case picture
        case cost
        case moq
        case mention(Mention)

        var title: String {
            switch self {
            case .picture: return "Picture".localized
            case .cost: return "Cost".localized
            case .moq: return "MOQ".localized
            case .mention: return "Mention".localized
            }
        }
    }

    var rows: [Row] {
        var rows: [Row] = [.picture, .cost, .moq]
        product?.mentions?.forEach({ (mention) in
            rows.append(.mention(mention as! Mention))
        })
        return rows
    }

    /** Создать контекст для работы с витриной */
    init(with product: Product?, in dataContext: NSManagedObjectContext, image: UIImage?, isModal: Bool = false) {
        self.product = product
        self.dataContext = dataContext
        self.image = image
        self.isModal = isModal
    }

    var activeResponder: UIResponder?

    /** Убираем клавиатуру */
    func resignResponder() {
        activeResponder?.resignFirstResponder()
        activeResponder = nil
    }

    func insertMention(_ style: MentionStyle) -> Mention {
        let mention = Mention.create(in: dataContext)
        mention.style = Int16(style.rawValue)
        product?.addToMentions(mention)
        return mention
    }

    func removeMention(at indexPath: IndexPath) {
        let index = indexPath.row - 3
        product?.removeFromMentions(at: index)
    }

    /** Валидация данных */
    func validate(_ completion: (String) -> Void) -> Bool {
        guard let name = product?.name, !name.isEmpty else {
            completion("Enter Details".localized)
            return false
        }
        return true
    }

}

extension ProductModifyDataContext: TableDataCollection {

    func registerCells(for tableView: UITableView) {
        PictureTableViewCellModel.registerCell(for: tableView)
        PriceTableViewCellModel.registerCell(for: tableView)
        TextFieldTableViewCellModel.registerCell(for: tableView)
        MentionTableViewCellProvider.registerCells(for: tableView)
    }

    func numberOfSections(for tableView: UITableView) -> Int {
        return 1
    }

    func numberOfRows(for tableView: UITableView, in section: Int) -> Int {
        return rows.count
    }

    func dequeueReusableCell(for tableView: UITableView, in viewController: UIViewController?, at indexPath: IndexPath) -> UITableViewCell {
        let row = rows[indexPath.row]
        switch row {
        case .picture:
            let cell = PictureTableViewCellModel.cell(for: tableView, at: indexPath)
            PictureTableViewCellModel.setup(cell, with: product?.picture, text: product?.name, delegate: self)
            return cell
        case .cost:
            let cell = PriceTableViewCellModel.cell(for: tableView, at: indexPath)
            PriceTableViewCellModel.setup(cell, title: row.title, cost: product?.cost, currency: product?.currency, delegate: self, at: indexPath)
            return cell
        case .moq:
            let cell = TextFieldTableViewCellModel.cell(for: tableView, at: indexPath)
            TextFieldTableViewCellModel.setup(cell, title: row.title, value: product?.quantity, style: .number, delegate: self, at: indexPath)
            return cell
        case .mention(let mention):
            let cell = MentionTableViewCellProvider.cell(for: tableView, with: mention, delegate: self, at: indexPath)
            return cell
        }
    }

    /**
     Выбор пункта меню.
     */
    func didSelect(for tableView: UITableView, in viewController: UIViewController?, at indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let row = rows[indexPath.row]
        switch row {
        case .mention(let mention):
            if mention.mentionStyle == .image {
                sketchCoordinator = SketchCoordinator.instantiate(with: mention.data as? UIImage, in: viewController) { [weak self] (image) in
                    if image != nil {
                        mention.data = image
                        if let tableViewController = viewController as? UITableViewController {
                            tableViewController.tableView.reloadData()
                        }
                    }
                    viewController?.dismiss(animated: true)
                    self?.sketchCoordinator = nil
                }
            }
        default:
            break
        }
    }

    func canEditRow(for tableView: UITableView, at indexPath: IndexPath) -> Bool {
        if indexPath.row > 2 {
            return true
        }
        return false
    }

}

extension ProductModifyDataContext: PictureTableViewCellDelegate {

    func pictureTableViewCell(_ cell: PictureTableViewCell, willSelect image: UIImage?, at indexPath: IndexPath?) {
        let viewController = AppRouter.topViewController()
        sketchCoordinator = SketchCoordinator.instantiate(with: product?.picture, in: viewController) { [weak self] (image) in
            if image != nil {
                self?.product?.picture = image
            }
            viewController?.dismiss(animated: true)
            self?.sketchCoordinator = nil
        }
    }

    func pictureTableViewCell(_ cell: PictureTableViewCell, didBeginEditing textView: UITextView, at indexPath: IndexPath?) {
        activeResponder = textView
    }

    func pictureTableViewCell(_ cell: PictureTableViewCell, didChange value: String?, at indexPath: IndexPath?) {
        product?.name = value
        activeResponder = nil
    }

}

extension ProductModifyDataContext: TextFieldTableViewCellDelegate {

    func textFieldTableViewCell(_ cell: TextFieldTableViewCell, didBeginEditing textField: UITextField, at indexPath: IndexPath?) {
        activeResponder = textField
    }

    func textFieldTableViewCell(_ cell: TextFieldTableViewCell, didChange value: String?, at indexPath: IndexPath?) {
        activeResponder = nil
        if let row = indexPath?.row {
            switch rows[row] {
            case .cost:
                if let value = value, !value.isEmpty {
                    product?.cost = value
                } else {
                    product?.cost = nil
                }
            case .moq:
                if let value = value, !value.isEmpty {
                    product?.quantity = value
                } else {
                    product?.quantity = nil
                }
            default:
                break
            }
        }
    }

}

extension ProductModifyDataContext: MentionTableViewCellDelegate {

    func mentionTableViewCell(_ cell: MentionTableViewCell, didBeginEditing responder: UIResponder, at indexPath: IndexPath?) {
        activeResponder = responder
    }

    func mentionTableViewCell(_ cell: MentionTableViewCell, didChange value: AnyObject?, at indexPath: IndexPath?) {
        activeResponder = nil
        if let indexPath = indexPath {
            let row = rows[indexPath.row]
            switch row {
            case .mention(let mention):
                switch mention.mentionStyle {
                case .property:
                    if var data = mention.data as? [String: String], let value = value as? String {
                        data["value"] = value
                        mention.data = data as AnyObject
                    } else {
                        mention.data = ["value": value] as AnyObject
                    }
                default:
                    mention.data = value
                }
            default:
                break
            }
        }
    }

}

extension ProductModifyDataContext: PriceTableViewCellDelegate {

    func priceTableViewCell(_ cell: PriceTableViewCell, didBeginEditing textField: UITextField, at indexPath: IndexPath?) {
        activeResponder = textField
    }

    func priceTableViewCell(_ cell: PriceTableViewCell, didChangeCost value: String?, at indexPath: IndexPath?) {
        product?.cost = value
    }

    func priceTableViewCell(_ cell: PriceTableViewCell, didChangeCurrency value: String?, at indexPath: IndexPath?) {
        product?.currency = value
    }

}
