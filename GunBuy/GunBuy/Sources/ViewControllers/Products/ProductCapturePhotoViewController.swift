/**

 Подготовка изображения для создания продукта

 */

import UIKit

class ProductCapturePhotoViewController: CaptureMediaViewController {

    @IBOutlet fileprivate weak var titleLabel: UILabel!

    @IBOutlet fileprivate weak var subtitleLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
    }

}
