
import UIKit
import Storage

class SketchDataContext {

    var image: UIImage?

    var product: Product?

    var title: String {
        return product?.name ?? "Image".localized
    }

    init(with image: UIImage?) {
        self.image = image
    }

}
