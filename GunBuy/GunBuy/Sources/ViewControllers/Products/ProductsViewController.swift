/**

 Продуткы: Товары и категории

 */

import UIKit

class ProductsViewController: UIViewController {

    @IBOutlet fileprivate var tableView: UITableView! {
        didSet {
            tableView.rowHeight = UITableView.automaticDimension
            tableView.estimatedRowHeight = 70.0
            tableView.tableFooterView = UIView()
            tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 100, right: 0)
            // Register NIB
            context.registerCells(for: tableView)
        }
    }

    @IBOutlet fileprivate var titleLabel: UILabel! {
        didSet {
            titleLabel.text = "No Products".localized
        }
    }

    @IBOutlet fileprivate var subtitleLabel: UILabel! {
        didSet {
            subtitleLabel.text = "Add Producrs and Products...".localized
        }
    }

    @IBOutlet fileprivate weak var sorrowImageView: UIImageView! {
        didSet {
            sorrowImageView.image = UIImage(style: .sorrow, frame: CGRect(x: 0, y: 0, width: 160, height: 160))
        }
    }

    @IBOutlet fileprivate var addButton: UIButton! {
        didSet {
            addButton.setBackgroundImage(UIImage(style: .roundedAdd, frame: CGRect(x: 0, y: 0, width: 480, height: 480)), for: .normal)
        }
    }

    @IBOutlet fileprivate var addLabel: UILabel! {
        didSet {
            addLabel.text = "New Product".localized
        }
    }

    /** Контекст данных */
    var context: ProductsDataContext!

    private struct SearchControllerRestorableState {
        var wasActive = false
        var wasFirstResponder = false
    }

    /// Search controller to help us with filtering.
    private var searchController: UISearchController!

    /// Restoration state for UISearchController
    private var restoredState = SearchControllerRestorableState()

    override func viewDidLoad() {
        super.viewDidLoad()
        title = context.title

        //MARK: - Search Controller
        let searchResultsController = SearchResultsController(context: context, presenter: self)
        searchController = UISearchController(searchResultsController: searchResultsController)
        searchController.searchResultsUpdater = self
        searchController.searchBar.autocapitalizationType = .none
        if #available(iOS 11.0, *) {
            navigationItem.searchController = searchController
            navigationItem.hidesSearchBarWhenScrolling = true
        } else {
            tableView.tableHeaderView = searchController.searchBar
        }
        //searchController.delegate = self
        searchController.dimsBackgroundDuringPresentation = true // The default is true.
        searchController.searchBar.delegate = self // Monitor when the search button is tapped.
        definesPresentationContext = true
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchData()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // Restore the searchController's active state.
        if restoredState.wasActive {
            searchController.isActive = restoredState.wasActive
            restoredState.wasActive = false

            if restoredState.wasFirstResponder {
                searchController.searchBar.becomeFirstResponder()
                restoredState.wasFirstResponder = false
            }
        }
    }

    /** Извлечение данных в контексте */
    func fetchData() {
        context.fetchData()
        reloadData()
    }

    /** Обновление данных */
    func reloadData() {
        tableView.reloadData()
        tableView.isHidden = context.isEmpty
    }

    @IBAction func doAdd(_ sender: UIBarButtonItem) {
        let shootViewController = UIStoryboard.Products.shoot(delegate: self).viewController
        present(shootViewController, animated: true)
    }

}

// MARK: - Table Source

extension ProductsViewController: UITableViewDelegate, UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return context.numberOfSections(for: tableView)
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return context.numberOfRows(for: tableView, in: section)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return context.dequeueReusableCell(for: tableView, in: self, at: indexPath)
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        context.didSelect(for: tableView, in: self, at: indexPath)
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return context.titleForHeader(for: tableView, in: section)
    }

}

extension ProductsViewController: CaptureMediaViewControllerDelegate {

    func captureMediaViewController(_ captureMediaViewController: CaptureMediaViewController?, didFinishWithImage image: UIImage?, preview: UIImage?) {
        context.createProduct(image: image, preview: preview, presenter: captureMediaViewController)
    }

    func captureMediaViewControllerWillCancel(_ captureMediaViewController: CaptureMediaViewController) {
        dismiss(animated: true)
    }

}

// MARK: - SearchController

extension ProductsViewController: UISearchBarDelegate {

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }

}

extension ProductsViewController: UISearchResultsUpdating {

    func updateSearchResults(for searchController: UISearchController) {
        context.updateSearchResults(with: searchController.searchBar.text!, in: searchController.searchResultsController!)
    }

}
