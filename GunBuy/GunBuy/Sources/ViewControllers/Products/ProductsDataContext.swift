/**

 Продуткы: Товары и категории

 */

import UIKit
import CoreData

let ProductsDataContextDidChangeSuccessfuly = Notification.Name("ProductsDataContextDidChangeSuccessfuly")

class ProductsDataContext {

    var showcase: Showcase?

    /** Контроллер для извлечения данных */
    var fetchedResultsController: NSFetchedResultsController<Product>!

    /** Контроллер доступа к базе */
    var dataController: CoreDataController!

    var title: String {
        return showcase?.name ?? "Products".localized
    }

    var isEmpty: Bool {
        return fetchedResultsController.sections!.isEmpty
    }

    init(with showcase: Showcase?, dataController: CoreDataController) {
        self.showcase = showcase
        self.dataController = dataController
        self.fetchedResultsController = Product.fetchedResultsController(with: dataController.viewContext)
    }

    /** Извлечение данных */
    func fetchData() {
        do {
            try fetchedResultsController.performFetch()
        } catch {
            print("Showcase fetch error")
        }
    }

}

extension ProductsDataContext: TableDataCollection {

    func registerCells(for tableView: UITableView) {
        ProductTableViewCellModel.registerCell(for: tableView)
    }

    func numberOfSections(for tableView: UITableView) -> Int {
        return fetchedResultsController.sections?.count ?? 0
    }

    func numberOfRows(for tableView: UITableView, in section: Int) -> Int {
        return fetchedResultsController.sections?[section].numberOfObjects ?? 0
    }

    func dequeueReusableCell(for tableView: UITableView, in viewController: UIViewController?, at indexPath: IndexPath) -> UITableViewCell {
        let cell = ProductTableViewCellModel.cell(for: tableView, at: indexPath)
        let product = fetchedResultsController.object(at: indexPath)
        ProductTableViewCellModel.setup(cell, with: product)
        return cell
    }

    func titleForHeader(for tableView: UITableView, in section: Int) -> String? {
        return fetchedResultsController.sections?[section].name
    }

    /**
     Выбор пункта меню.
     */
    func didSelect(for tableView: UITableView, in viewController: UIViewController?, at indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let product = fetchedResultsController.object(at: indexPath)
        let dataContext = dataController.newBackgroundContext()
        viewController?.navigationController?.pushViewController(UIStoryboard.Products.modify(product: product, dataContext: dataContext, image: nil, isModal: false, delegate: self).viewController, animated: true)
    }

}

extension ProductsDataContext {

    func updateSearchResults(with searchText: String, in viewController: UIViewController) {
        let request = fetchedResultsController.fetchRequest
        if searchText.isEmpty {
            request.predicate = nil
        } else {
            request.predicate = NSPredicate(format: "name contains[c] %@", searchText)
        }
        fetchData()

        if let resultsController = viewController as? UITableViewController {
            resultsController.tableView.reloadData()
        }
    }

}

extension ProductsDataContext {

    func createProduct(image: UIImage?, preview: UIImage?, presenter: UIViewController?) {
        // Создаем товар
        let dataContext = dataController.newBackgroundContext()
        let product = Product.create(in: dataContext)
        product.picture = preview

        let modifyViewController = UIStoryboard.Products.modify(product: product, dataContext: dataContext, image: image, isModal: false, delegate: self).viewController
        //presenter.present(modifyViewController, animated: true)
        presenter?.navigationController?.pushViewController(modifyViewController, animated: true)
    }

}

extension ProductsDataContext: ProductModifyViewControllerDelegate {

    func productModifyViewController(_ productModifyViewController: ProductModifyViewController, didChange product: Product?, in dataContext: NSManagedObjectContext) {
        dataController.save(changesIn: dataContext)
        NotificationCenter.default.post(name: ProductsDataContextDidChangeSuccessfuly, object: nil)
        productModifyViewController.dismiss(animated: true)
    }

    func productModifyViewControllerWillCancel(_ productModifyViewController: ProductModifyViewController) {
        productModifyViewController.dismiss(animated: true)
    }
    
}
