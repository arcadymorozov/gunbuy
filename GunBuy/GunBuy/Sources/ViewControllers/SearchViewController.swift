/**

 Базовый контроллер для организации поиска

 */

import UIKit

class SearchViewController: UIViewController {

    var context: TableDataCollection!

    private struct SearchControllerRestorableState {
        var wasActive = false
        var wasFirstResponder = false
    }

    /// Search controller to help us with filtering.
    private var searchController: UISearchController!

    /// Restoration state for UISearchController
    private var restoredState = SearchControllerRestorableState()

    override func viewDidLoad() {
        super.viewDidLoad()
        addSearchController()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // Restore the searchController's active state.
        if restoredState.wasActive {
            searchController.isActive = restoredState.wasActive
            restoredState.wasActive = false

            if restoredState.wasFirstResponder {
                searchController.searchBar.becomeFirstResponder()
                restoredState.wasFirstResponder = false
            }
        }
    }

    func addSearchController() {
        //MARK: - Search Controller
        let searchResultsController = SearchResultsController(context: context, presenter: self)
        searchController = UISearchController(searchResultsController: searchResultsController)
        searchController.searchResultsUpdater = self
        searchController.searchBar.autocapitalizationType = .none
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = true
        //searchController.delegate = self
        searchController.dimsBackgroundDuringPresentation = true // The default is true.
        searchController.searchBar.delegate = self // Monitor when the search button is tapped.
        definesPresentationContext = true
    }

}

extension SearchViewController: UISearchBarDelegate {

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }

}

extension SearchViewController: UISearchResultsUpdating {

    func updateSearchResults(for searchController: UISearchController) {
        context.updateSearchResults(with: searchController.searchBar.text!, in: searchController.searchResultsController!)
    }

}
