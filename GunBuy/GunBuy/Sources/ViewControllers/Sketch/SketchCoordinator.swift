/**

 Организовывает работы Mention со Sketch

    Принимает замыкание, которое выполняется

 */

import UIKit

typealias SketchCoordinatorHandler = (UIImage?) -> ()

class SketchCoordinator {

    var handler: SketchCoordinatorHandler

    init(handler: @escaping SketchCoordinatorHandler) {
        self.handler = handler
    }

    static func instantiate(with image: UIImage?, in viewController: UIViewController?, completion: @escaping SketchCoordinatorHandler) -> SketchCoordinator {
        print("instantiate SketchCoordinator")
        let sketchCoordinator = SketchCoordinator(handler: completion)
        let sketchViewController = UIStoryboard.Sketch.main(image: image, delegate: sketchCoordinator).viewController
        viewController?.present(sketchViewController, animated: true)
        return sketchCoordinator
    }

    deinit {
        print("deinit SketchCoordinator")
    }

}

extension SketchCoordinator: SketchViewControllerDelegate {

    func sketchViewController(_ sketchViewController: SketchViewController, didChange image: UIImage?) {
        handler(image)
    }

    func sketchViewControllerWillCancel(_ sketchViewController: SketchViewController) {
        handler(nil)
    }

}
