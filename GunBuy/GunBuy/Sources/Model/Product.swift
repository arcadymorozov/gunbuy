/**

 Товарная категория / Товар

 */

import Foundation
import CoreData

enum ProductStyle: Int {
    case product = 0
    case category = 1

    var title: String {
        switch self {
        case .product: return "Product".localized
        case .category: return "Category".localized
        }
    }
}

@objc(Product)
public class Product: CoreObject {

    var productStyle: ProductStyle? {
        return ProductStyle(rawValue: Int(style))
    }

    var storagePath: URL? {
        let path = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
        return path?.appendingPathComponent(self.identifier!, isDirectory: true)
    }

    /**
     Создание нового Product

     - parameter context: Контекст, в котором создается Product
     */
    class func create(in context: NSManagedObjectContext) -> Product {
        let product = Product(context: context)
        // Sync fields
        product.identifier = UUID().uuidString
        product.lastModifiedDate = Date()
        product.didCreate()
        product.didChange()
        // Default Fields
        return product
    }

    /**
     Поиск Showcase в контексте. Если не найден, возвращается новосозданный

     - parameter identifier: Идентификатов Showcase
     - parameter context: Контекст, в котором создается Showcase
     */
    class func product(identifier: String, in context: NSManagedObjectContext) -> Product {
        let request: NSFetchRequest<Product> = fetchRequest()
        request.predicate = NSPredicate(format: "identifier = %@", identifier)

        if let products = try? context.fetch(request), let product = products.first {
            // Showcase сущеуствует
            return product
        } else {
            // Showcase не существует, создадим
            return Product.create(in: context)
        }
    }

    /**
     Контроллер извлечения данных

     - parameter context: Контекст данных
     */
    class func fetchedResultsController(with context: NSManagedObjectContext, supplier: Supplier? = nil) -> NSFetchedResultsController<Product> {
        let request: NSFetchRequest<Product> = fetchRequest()
        request.sortDescriptors = [NSSortDescriptor(key: "style", ascending: true), NSSortDescriptor(key: "lastModifiedDate", ascending: false)]
        var sectionNameKeyPath: String? = nil
        if let supplier = supplier {
            request.predicate = NSPredicate(format: "supplier == %@", supplier)
        } else {
            sectionNameKeyPath = "supplier.name"
        }
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: request, managedObjectContext: context, sectionNameKeyPath: sectionNameKeyPath, cacheName: nil)
        return fetchedResultsController
    }

}
