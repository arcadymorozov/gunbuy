//
//  Attributes+CoreDataProperties.swift
//  
//
//  Created by Arcady Morozov on 20/09/2019.
//
//

import Foundation
import CoreData


extension Attributes {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Attributes> {
        return NSFetchRequest<Attributes>(entityName: "Attributes")
    }

    @NSManaged public var name: String?
    @NSManaged public var value: String?

}
