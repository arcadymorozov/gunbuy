//
//  Showcase+CoreDataProperties.swift
//  
//
//  Created by Arcady Morozov on 20/09/2019.
//
//

import Foundation
import CoreData


extension Showcase {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Showcase> {
        return NSFetchRequest<Showcase>(entityName: "Showcase")
    }

    @NSManaged public var identifier: String?
    @NSManaged public var lastModifiedDate: Date?

    @NSManaged public var name: String?
    @NSManaged public var details: String?

    @NSManaged public var style: Int16

    @NSManaged public var suppliers: Set<Supplier>?

    @NSManaged public var attributes: String?

}
