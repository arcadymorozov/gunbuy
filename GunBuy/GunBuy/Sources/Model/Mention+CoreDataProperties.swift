//
//  Mention+CoreDataProperties.swift
//  
//
//  Created by Arcady Morozov on 20/09/2019.
//
//

import Foundation
import CoreData


extension Mention {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Mention> {
        return NSFetchRequest<Mention>(entityName: "Mention")
    }

    @NSManaged public var identifier: String?
    @NSManaged public var lastModifiedDate: Date?

    @NSManaged public var data: AnyObject?

    @NSManaged public var style: Int16

    @NSManaged public var products: Set<Product>?

}
