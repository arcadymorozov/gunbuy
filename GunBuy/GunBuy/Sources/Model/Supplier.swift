/**

 Производитель

*/

import Foundation
import CoreData

@objc(Supplier)
public class Supplier: CoreObject {

    /**
     Создание нового Производителя

     - parameter context: Контекст, в котором создается Showcase
     */
    class func create(in context: NSManagedObjectContext) -> Supplier {
        let supplier = Supplier(context: context)
        // Sync fields
        supplier.identifier = UUID().uuidString
        supplier.lastModifiedDate = Date()
        supplier.didCreate()
        supplier.didChange()
        // Default Fields
        return supplier
    }

    /**
     Поиск Showcase в контексте. Если не найден, возвращается новосозданный

     - parameter identifier: Идентификатов Showcase
     - parameter context: Контекст, в котором создается Showcase
     */
    class func supplier(identifier: String, in context: NSManagedObjectContext) -> Supplier {
        let request: NSFetchRequest<Supplier> = fetchRequest()
        request.predicate = NSPredicate(format: "identifier = %@", identifier)

        if let suppliers = try? context.fetch(request), let supplier = suppliers.first {
            // Производитель сущеуствует
            return supplier
        } else {
            // Производитель не существует, создадим
            return Supplier.create(in: context)
        }
    }

    /**
     Контроллер извлечения данных

     - parameter context: Контекст данных
     */
    class func fetchedResultsController(with context: NSManagedObjectContext, showcase: Showcase?) -> NSFetchedResultsController<Supplier> {
        let request: NSFetchRequest<Supplier> = fetchRequest()
        request.sortDescriptors = [NSSortDescriptor(key: "lastModifiedDate", ascending: false)]
        if let showcase = showcase {
            request.predicate = NSPredicate(format: "showcase == %@", showcase)
        }
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: request, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
        return fetchedResultsController
    }

}
