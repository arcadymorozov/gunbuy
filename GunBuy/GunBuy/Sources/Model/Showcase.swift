//
//  Showcase+CoreDataClass.swift
//  
//
//  Created by Arcady Morozov on 20/09/2019.
//
//

import Foundation
import CoreData

enum ShowcaseStyle: Int {
    case unspecified = 0
    case exhibition = 1
    case market = 2
    case factory = 3
    case wholesale = 4

    var tag: Int {
        return self.rawValue
    }

    var name: String {
        switch self {
        case .unspecified: return ""
        case .exhibition: return "Exhibition".localized
        case .market: return "Market".localized
        case .factory: return "Factory".localized
        case .wholesale: return "Wholesale".localized
        }
    }

    var title: String {
        switch self {
        case .unspecified: return ""
        case .exhibition: return "Exhibitions".localized
        case .market: return "Markets".localized
        case .factory: return "Factorys".localized
        case .wholesale: return "Wholesalers".localized
        }
    }
}

@objc(Showcase)
public class Showcase: CoreObject {

    var showcaseStyle: ShowcaseStyle? {
        return ShowcaseStyle(rawValue: Int(style))
    }

    /**
     Создание нового Showcase

     - parameter context: Контекст, в котором создается Showcase
     */
    class func create(in context: NSManagedObjectContext) -> Showcase {
        let showcase = Showcase(context: context)
        // Sync fields
        showcase.identifier = UUID().uuidString
        showcase.lastModifiedDate = Date()
        showcase.didCreate()
        showcase.didChange()
        // Default Fields
        return showcase
    }

    /**
     Поиск Showcase в контексте. Если не найден, возвращается новосозданный

     - parameter identifier: Идентификатов Showcase
     - parameter context: Контекст, в котором создается Showcase
     */
    class func showcase(identifier: String, in context: NSManagedObjectContext) -> Showcase {
        let request: NSFetchRequest<Showcase> = fetchRequest()
        request.predicate = NSPredicate(format: "identifier = %@", identifier)

        if let showcases = try? context.fetch(request), let showcase = showcases.first {
            // Showcase сущеуствует
            return showcase
        } else {
            // Showcase не существует, создадим
            return Showcase.create(in: context)
        }
    }

    /**
     Контроллер извлечения данных

     - parameter context: Контекст данных
     */
    class func fetchedResultsController(with context: NSManagedObjectContext) -> NSFetchedResultsController<Showcase> {
        let request: NSFetchRequest<Showcase> = fetchRequest()
        request.sortDescriptors = [NSSortDescriptor(key: "style", ascending: true), NSSortDescriptor(key: "lastModifiedDate", ascending: false)]
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: request, managedObjectContext: context, sectionNameKeyPath: "style", cacheName: nil)
        return fetchedResultsController
    }

}
