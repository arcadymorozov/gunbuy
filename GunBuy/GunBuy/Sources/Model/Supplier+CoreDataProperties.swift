/**

 Производитель

 */

import UIKit
import CoreData


extension Supplier {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Supplier> {
        return NSFetchRequest<Supplier>(entityName: "Supplier")
    }

    @NSManaged public var identifier: String?
    @NSManaged public var lastModifiedDate: Date?

    @NSManaged public var name: String?
    @NSManaged public var details: String?

    @NSManaged public var picture: UIImage?

    @NSManaged public var products: Set<Product>?
    @NSManaged public var showcase: Showcase?

    @NSManaged public var mentions: NSOrderedSet?

}

// MARK: Generated accessors for mentions
extension Supplier {

    @objc(insertObject:inMentionsAtIndex:)
    @NSManaged public func insertIntoMentions(_ value: Mention, at idx: Int)

    @objc(removeObjectFromMentionsAtIndex:)
    @NSManaged public func removeFromMentions(at idx: Int)

    @objc(insertMentions:atIndexes:)
    @NSManaged public func insertIntoMentions(_ values: [Mention], at indexes: NSIndexSet)

    @objc(removeMentionsAtIndexes:)
    @NSManaged public func removeFromMentions(at indexes: NSIndexSet)

    @objc(replaceObjectInMentionsAtIndex:withObject:)
    @NSManaged public func replaceMentions(at idx: Int, with value: Mention)

    @objc(replaceMentionsAtIndexes:withMentions:)
    @NSManaged public func replaceMentions(at indexes: NSIndexSet, with values: [Mention])

    @objc(addMentionsObject:)
    @NSManaged public func addToMentions(_ value: Mention)

    @objc(removeMentionsObject:)
    @NSManaged public func removeFromMentions(_ value: Mention)

    @objc(addMentions:)
    @NSManaged public func addToMentions(_ values: NSOrderedSet)

    @objc(removeMentions:)
    @NSManaged public func removeFromMentions(_ values: NSOrderedSet)

}
