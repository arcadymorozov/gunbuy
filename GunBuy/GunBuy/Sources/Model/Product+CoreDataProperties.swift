//
//  Product+CoreDataProperties.swift
//  
//
//  Created by Arcady Morozov on 04/10/2019.
//
//

import UIKit
import CoreData


extension Product {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Product> {
        return NSFetchRequest<Product>(entityName: "Product")
    }

    @NSManaged public var cost: String?
    @NSManaged public var currency: String?
    @NSManaged public var details: String?
    @NSManaged public var identifier: String?
    @NSManaged public var lastModifiedDate: Date?
    @NSManaged public var name: String?
    @NSManaged public var picture: UIImage?
    @NSManaged public var quantity: String?
    @NSManaged public var style: Int16
    @NSManaged public var mentions: NSOrderedSet?
    @NSManaged public var mentor: Product?
    @NSManaged public var products: Set<Product>?
    @NSManaged public var supplier: Supplier?

}

// MARK: Generated accessors for mentions
extension Product {

    @objc(insertObject:inMentionsAtIndex:)
    @NSManaged public func insertIntoMentions(_ value: Mention, at idx: Int)

    @objc(removeObjectFromMentionsAtIndex:)
    @NSManaged public func removeFromMentions(at idx: Int)

    @objc(insertMentions:atIndexes:)
    @NSManaged public func insertIntoMentions(_ values: [Mention], at indexes: NSIndexSet)

    @objc(removeMentionsAtIndexes:)
    @NSManaged public func removeFromMentions(at indexes: NSIndexSet)

    @objc(replaceObjectInMentionsAtIndex:withObject:)
    @NSManaged public func replaceMentions(at idx: Int, with value: Mention)

    @objc(replaceMentionsAtIndexes:withMentions:)
    @NSManaged public func replaceMentions(at indexes: NSIndexSet, with values: [Mention])

    @objc(addMentionsObject:)
    @NSManaged public func addToMentions(_ value: Mention)

    @objc(removeMentionsObject:)
    @NSManaged public func removeFromMentions(_ value: Mention)

    @objc(addMentions:)
    @NSManaged public func addToMentions(_ values: NSOrderedSet)

    @objc(removeMentions:)
    @NSManaged public func removeFromMentions(_ values: NSOrderedSet)

}
