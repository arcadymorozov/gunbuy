//
//  Event+CoreDataProperties.swift
//  
//
//  Created by Arcady Morozov on 20/09/2019.
//
//

import Foundation
import CoreData


extension Event {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Event> {
        return NSFetchRequest<Event>(entityName: "Event")
    }

    @NSManaged public var endDate: Date?
    @NSManaged public var isAllDay: Bool
    @NSManaged public var startDate: Date?

}
