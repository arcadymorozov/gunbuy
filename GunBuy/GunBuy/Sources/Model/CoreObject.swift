/**

 Базовый объект модели.
 Все сущности данных наследуются от CoreObject

 Включает методы нотификации об изменении модели

 */

import CoreData

let CoreObjectDidCreateNotification = Notification.Name("CoreObjectDidCreateNotification")
let CoreObjectWillChangeNotification = Notification.Name("CoreObjectWillChangeNotification")
let CoreObjectDidChangeNotification = Notification.Name("CoreObjectDidChangeNotification")

public class CoreObject: NSManagedObject {

    /** Действия, выполняемые после создания */
    func didCreate() {
        NotificationCenter.default.post(name: CoreObjectDidCreateNotification, object: self)
    }

    /** Действия, выполняемые перед изменениями */
    func willChange() {
        NotificationCenter.default.post(name: CoreObjectWillChangeNotification, object: self)
    }

    /** Действия, выполняемые после изменения */
    func didChange() {
        NotificationCenter.default.post(name: CoreObjectDidChangeNotification, object: self)
    }

}
