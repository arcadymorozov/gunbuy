/**

 Медиа сущность.

 */

import Foundation
import CoreData

enum MentionStyle: Int {
    case text = 0
    case image = 1
    case video = 2
    case property = 3

    var tag: Int {
        return self.rawValue
    }
}

@objc(Mention)
public class Mention: CoreObject {

    var mentionStyle: MentionStyle? {
        return MentionStyle(rawValue: Int(style))
    }

    /**
     Создание нового Showcase

     - parameter context: Контекст, в котором создается Showcase
     */
    class func create(in context: NSManagedObjectContext) -> Mention {
        let mention = Mention(context: context)
        // Sync fields
        mention.identifier = UUID().uuidString
        mention.lastModifiedDate = Date()
        mention.didCreate()
        mention.didChange()
        // Default Fields
        return mention
    }

}
