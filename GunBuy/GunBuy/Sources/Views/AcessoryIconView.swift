/**

 Стрелка вправо

 */

import UIKit

class AcessoryIconView: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.clear
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func draw(_ rect: CGRect) {
        let сolor = UIColor(red: 0.000, green: 0.478, blue: 1.000, alpha: 1.000)

        //// Bezier Drawing
        let bezierPath = UIBezierPath()
        bezierPath.move(to: CGPoint(x: rect.minX + 0.67339 * rect.width, y: rect.minY + 0.15900 * rect.height))
        bezierPath.addCurve(to: CGPoint(x: rect.minX + 0.62299 * rect.width, y: rect.minY + 0.15900 * rect.height), controlPoint1: CGPoint(x: rect.minX + 0.65976 * rect.width, y: rect.minY + 0.14490 * rect.height), controlPoint2: CGPoint(x: rect.minX + 0.63709 * rect.width, y: rect.minY + 0.14490 * rect.height))
        bezierPath.addCurve(to: CGPoint(x: rect.minX + 0.62299 * rect.width, y: rect.minY + 0.20889 * rect.height), controlPoint1: CGPoint(x: rect.minX + 0.60937 * rect.width, y: rect.minY + 0.17263 * rect.height), controlPoint2: CGPoint(x: rect.minX + 0.60937 * rect.width, y: rect.minY + 0.19530 * rect.height))
        bezierPath.addLine(to: CGPoint(x: rect.minX + 0.87853 * rect.width, y: rect.minY + 0.46443 * rect.height))
        bezierPath.addLine(to: CGPoint(x: rect.minX + 0.03528 * rect.width, y: rect.minY + 0.46443 * rect.height))
        bezierPath.addCurve(to: CGPoint(x: rect.minX + 0.00000 * rect.width, y: rect.minY + 0.49975 * rect.height), controlPoint1: CGPoint(x: rect.minX + 0.01562 * rect.width, y: rect.minY + 0.46446 * rect.height), controlPoint2: CGPoint(x: rect.minX + 0.00000 * rect.width, y: rect.minY + 0.48009 * rect.height))
        bezierPath.addCurve(to: CGPoint(x: rect.minX + 0.03528 * rect.width, y: rect.minY + 0.53553 * rect.height), controlPoint1: CGPoint(x: rect.minX + 0.00000 * rect.width, y: rect.minY + 0.51940 * rect.height), controlPoint2: CGPoint(x: rect.minX + 0.01562 * rect.width, y: rect.minY + 0.53553 * rect.height))
        bezierPath.addLine(to: CGPoint(x: rect.minX + 0.87853 * rect.width, y: rect.minY + 0.53553 * rect.height))
        bezierPath.addLine(to: CGPoint(x: rect.minX + 0.62299 * rect.width, y: rect.minY + 0.79060 * rect.height))
        bezierPath.addCurve(to: CGPoint(x: rect.minX + 0.62299 * rect.width, y: rect.minY + 0.84099 * rect.height), controlPoint1: CGPoint(x: rect.minX + 0.60937 * rect.width, y: rect.minY + 0.80470 * rect.height), controlPoint2: CGPoint(x: rect.minX + 0.60937 * rect.width, y: rect.minY + 0.82740 * rect.height))
        bezierPath.addCurve(to: CGPoint(x: rect.minX + 0.67339 * rect.width, y: rect.minY + 0.84099 * rect.height), controlPoint1: CGPoint(x: rect.minX + 0.63709 * rect.width, y: rect.minY + 0.85509 * rect.height), controlPoint2: CGPoint(x: rect.minX + 0.65980 * rect.width, y: rect.minY + 0.85509 * rect.height))
        bezierPath.addLine(to: CGPoint(x: rect.minX + 0.98942 * rect.width, y: rect.minY + 0.52496 * rect.height))
        bezierPath.addCurve(to: CGPoint(x: rect.minX + 0.98942 * rect.width, y: rect.minY + 0.47507 * rect.height), controlPoint1: CGPoint(x: rect.minX + 1.00353 * rect.width, y: rect.minY + 0.51133 * rect.height), controlPoint2: CGPoint(x: rect.minX + 1.00353 * rect.width, y: rect.minY + 0.48866 * rect.height))
        bezierPath.addLine(to: CGPoint(x: rect.minX + 0.67339 * rect.width, y: rect.minY + 0.15900 * rect.height))
        bezierPath.close()
        сolor.setFill()
        bezierPath.fill()
    }

}
