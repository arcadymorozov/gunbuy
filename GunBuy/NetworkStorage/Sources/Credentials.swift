/**
 
 Текущая сессия.
 
 Класс хранит текущаю сессию.
 Данные пользователя сохраняются локально,
 имя файла задается в credentialsFileName
 
 */

import Foundation

class Credentials {
    
    /** Сессия пользователя */
    var token: String?
    
    init(with token: String?) {
        self.token = token
    }
    
    /** Возвращает True если текущая сессия активна */
    func isAuthorized() -> Bool {
        return token != nil
    }
    
    /** Локальное сохранение данных пользователя */
    class func save(_ credentials: Credentials) {
        if let token = credentials.token {
            let params: [String : Any] = [
                Credentials.JsonKey.token.rawValue: token
            ]
            if let data = try? JSONSerialization.data(withJSONObject: params), let jsonString = String(data: data, encoding: String.Encoding.utf8), let path = documentDirectoryURL(for: Credentials.credentialsFileName) {
                try? jsonString.write(to: path, atomically: true, encoding: String.Encoding.utf8)
            }
        }
    }
    
    /** Загрузка локальных данных пользователя */
    class func load() -> Credentials? {
        if let data = loadData(),
            let token = data[Credentials.JsonKey.token.rawValue] as? String {
            return Credentials(with: token)
        }
        return nil
    }
    
    /** Удаление локальных данных пользователя */
    class func remove() {
        if let path = Credentials.documentDirectoryURL(for: Credentials.credentialsFileName) {
            try? FileManager.default.removeItem(at: path)
        }
    }
    
    // MARK: -
    
    /** Имя файла для сохранения данных пользователя */
    private static let credentialsFileName = "credentials.json"
    
    /** Список ключей для файла с данными пользователя */
    private enum JsonKey: String {
        case token
    }
    
    // MARK: -
    
    private class func documentDirectoryURL(for resource: String) -> URL? {
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            return dir.appendingPathComponent(resource)
        }
        return nil
    }
    
    private class func loadData() -> [String: Any]? {
        if let path = documentDirectoryURL(for: Credentials.credentialsFileName) {
            if let data = try? Data(contentsOf: path),
                let json = (try? JSONSerialization.jsonObject(with: data)) as? [String: Any] {
                return json
            }
        }
        return nil
    }
    
}
