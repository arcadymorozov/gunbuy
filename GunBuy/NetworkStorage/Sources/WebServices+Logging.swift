/**
 
Logging
 
 */


import Alamofire
import SwiftyJSON

extension WebServices {
    
    func log(_ request: URLRequest) {
        print("---REQUEST---")
        print("url: \(request)")
        if let allHeaders = request.allHTTPHeaderFields {
            print("headers: \(allHeaders)")
        } else {
            print("headers: empty")
        }
        if let body = request.httpBody,
            let decodedBody = NSString(data: body, encoding: String.Encoding.utf8.rawValue) {
            print("request body: \(decodedBody)")
        } else {
            print("request body: empty")
        }
        print("-------------")
    }
    
    func log(_ response: DataResponse<Any>) {
        print("---RESPONSE--")
        if let url = response.request?.url {
            print("url: \(url)")
        }
        if let dictionary = response.result.value as? [String: Any] {
            print("response values: \(dictionary)")
        }
        print("-------------")
    }
    
}
