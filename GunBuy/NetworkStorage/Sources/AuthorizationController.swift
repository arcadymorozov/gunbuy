/**
 
 AuthorizationController
 
 Авторизация пользователя
 
 */

import Foundation

/** Параметры авторизации
 Bool: статус успешности авторизации
 Int: идентификатор пользователя
 String: сообщение об ощибке, что ли..
 */
public typealias AuthorizationCompletionHandler = ((Bool, String?) -> Void)

public class AuthorizationController {
    
    /**
     Авторизация текущего пользователя
     - returns: True, если пользователь авторизован
     */
    public class func isAuthorized() -> Bool {
        if let credentials = WebServices.main.credentials {
            return credentials.isAuthorized()
        }
        return false
    }
    
    /**
     Авторизация пользователя по параметрам логин - пароль
     
     - parameter login: mobile - имя пользователя
     - parameter password: 1111111111 - пароль
     - parameter completion: Блок завершения
     - returns: none
     */
    public class func authorize(with login: String, password: String, completion: AuthorizationCompletionHandler?) {
        WebServices.main.sendRequest(with: WebServices.API.URL.login, method: .post, parameters: [WebServices.API.JSONKey.login.rawValue: login, WebServices.API.JSONKey.password.rawValue: password]) { (dictionary, success) in
            if success {
                // Успешно залогинились
                if let token = dictionary?[WebServices.API.JSONKey.token.rawValue] as? String {
                    let credentials = Credentials(with: token)
                    WebServices.main.credentials = credentials
                    Credentials.save(credentials)
                    completion?(true, nil)
                }
            } else {
                // Ошибка при авторизации
                if let error = dictionary?[WebServices.API.RequestResult.error.rawValue] as? Error {
                    // Ошибка при выполнении запроса
                    completion?(false, error.localizedDescription)
                } else {
                    completion?(false, nil)
                }
            }
        }
    }
    
    public class func logout() {
        WebServices.main.removeCredentials()
    }
    
}
