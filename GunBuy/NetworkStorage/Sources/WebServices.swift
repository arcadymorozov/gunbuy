/**
 
 WebServices
 
 Сетевые сервисы.
 
 */

import Foundation
import Alamofire
import SwiftyJSON

public class WebServices {
    
    public static let main = WebServices()
    
    enum API: String {
        /** Базовый адрес АПИ */
        case baseURL = "https://speshu.marketstage.ru/api/v1/"
        
        /** Запросы к АПИ */
        enum URL: String {
            case login
            case registration
            
            var path: String {
                return API.baseURL.rawValue + self.rawValue
            }
        }
        
        /** Поля приходящие в ответах */
        enum JSONKey: String {
            // Ответы сервера
            case status
            case message

            case login
            case password
            case token

            case user
        }
        
        /** Значение поля результат */
        enum RequestResult: String {
            case success
            case error
        }
    }
    
    let applicationHeaders: HTTPHeaders = ["User-Agent": "iOS", "Content-Type": "application/json"]
    
    let multipartHeaders: HTTPHeaders = ["User-Agent": "iOS", "Content-Type": "multipart/form-data"]
    
    var credentials: Credentials?
    
    func loadCredentials() -> Bool {
        guard let localCredentials = Credentials.load() else {
            return false
        }
        credentials = localCredentials
        return true
    }
    
    func removeCredentials() {
        Credentials.remove()
        credentials = nil
    }
    
    /**
     Выполнение запроса к серверу
     
     - parameter url: API.URL адрес запроса
     - parameter method: метод запроса
     - parameter parameters: данные передаваемые в запросе
     - parameter completion: Блок завершения [String: Any] данные, полученные от сервера, Bool статус операции
     - returns: none
     */
    func sendRequest(with url: WebServices.API.URL, method: HTTPMethod = .post, parameters: Parameters = [:], completion: @escaping ([String: Any]?, Bool) -> Void) {
        
        guard let json = try? JSONSerialization.data(withJSONObject: appendCredentials(parameters)) else {
            return
        }
        
        // -- application/json --
        
        guard var request = try? URLRequest(url: url.path, method: method, headers: applicationHeaders) else {
            return
        }

        request.httpBody = json
        log(request)
        
        Alamofire.request(request).validate().responseJSON { (response) in
            handleResponse(response: response)
        }
        
        // -- application/json --
        
        func handleResponse(response: DataResponse<Any>) {
            self.log(response)
            
            switch response.result {
            case .success:
                if let dictionary = response.result.value as? [String: Any] {
/*
                    if let result = dictionary[API.JSONKey.result.rawValue] as? String {
                        switch API.RequestResult(rawValue: result) ?? .error {
                        case .success:
                            completion(dictionary, true)
                            break
                        case .error:
                            if let message = dictionary[API.JSONKey.message.rawValue] as? String {
                                // Сервер сообщил об ошибке в данных..
                                let error = NSError(domain: "WebServices", code: 0, userInfo: [NSLocalizedDescriptionKey : message])
                                completion([API.RequestResult.error.rawValue: error, "message": message], false)
                            } else {
                                completion(dictionary, false)
                            }
                            break
                        }
                    }
                        
                        /// Заглушка для результатов поиска
                    else if (dictionary[API.JSONKey.result.rawValue] as? [[String: Any]]) != nil {
                        // Получили список результатову
                        completion(dictionary, true)
                    }
                        
                        /// Заглушка для списков
                    else if (dictionary[API.JSONKey.items.rawValue] as? [[String: Any]]) != nil {
                        // Получили список
                        completion(dictionary, true)
                    }
                        
                    else if (dictionary[API.JSONKey.results.rawValue] as? [String: Any]) != nil {
                        completion(dictionary, true)
                    }
                        
                    else if (dictionary[API.JSONKey.results.rawValue] as? [String]) != nil {
                        completion(dictionary, true)
                    }
                        
                    else if (dictionary[API.JSONKey.message.rawValue] as? [[String: Any]]) != nil {
                        completion(dictionary, true)
                    }
                        
                        //сюда падает результат запроса категорий ТС в транспортном калькуляторе
                    else if (dictionary[API.JSONKey.car_categories.rawValue] as? [[String: Any]]) != nil {
                        completion(dictionary, true)
                    }
                        //сюда падает результат запроса мест отправления в транспортном калькуляторе
                        //сюда же падает запрос мест назначения в транспортном калькуляторе
                    else if (dictionary[API.JSONKey.cities.rawValue] as? [[String: Any]]) != nil {
                        completion(dictionary, true)
                    }
                        //сюда падает результат расчета стоимости доставки
                    else if (dictionary[API.JSONKey.calc.rawValue] as? [[String: Any]]) != nil {
                        completion(dictionary, true)
                    }
*/
                } else {
                    let error = ParsingError(parsedValue: response.result.value)
                    handle(error: error)
                    completion([API.RequestResult.error.rawValue: error], false)
                }
            case .failure(let error):
                handle(error: error)
                completion([API.RequestResult.error.rawValue: error], false)
            }
        }
        
    }
    
    /**
     Добавляет к списку параметров авторизационный токен
     */
    private func appendCredentials(_ parameters: Parameters) -> Parameters {
        var result = parameters
        if let credentials = credentials {
            result[API.JSONKey.token.rawValue] = credentials.token
        }
        return result
    }
    
    /**
     Обработка ощибок запроса
     */
    private func handle(error: Error) {
        print("error = \(error)")
    }
    
    // MARK: - Генерируемые ошибка
    
    private struct ParsingError: Error {
        let parsedValue: Any?
        init(parsedValue: Any?) {
            self.parsedValue = parsedValue
        }
    }

}
